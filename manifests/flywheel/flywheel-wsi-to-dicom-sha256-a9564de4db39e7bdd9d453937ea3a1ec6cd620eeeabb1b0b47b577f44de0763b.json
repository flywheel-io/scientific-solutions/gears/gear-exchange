{
  "exchange": {
    "git-commit": "ff09202071fb36b70d2a0d4a84f91e0d86375341",
    "rootfs-hash": "sha256:a9564de4db39e7bdd9d453937ea3a1ec6cd620eeeabb1b0b47b577f44de0763b",
    "rootfs-url": "docker://us-docker.pkg.dev/flywheel-exchange/gear-exchange/flywheel-wsi-to-dicom:0.2.1_1.0.3"
  },
  "gear": {
    "author": "Flywheel",
    "cite": "OpenSlide: A Vendor-Neutral Software Foundation for Digital Pathology\nAdam Goode, Benjamin Gilbert, Jan Harkes, Drazen Jukic, M. Satyanarayanan\nJournal of Pathology Informatics 2013, 4:27",
    "command": "python run.py",
    "config": {
      "batch": {
        "default": -1,
        "description": "Maximum frames in one file, as limit is exceeded new files is started (\"No Maximum\" is -1).",
        "type": "integer"
      },
      "compression": {
        "default": "jpeg",
        "description": "Compression, supported compressions: jpeg, jpeg2000, raw.",
        "enum": [
          "jpeg",
          "jpeg2000",
          "raw"
        ],
        "type": "string"
      },
      "debug": {
        "default": false,
        "description": "Print debug messages: dimensions of levels, size of frames.",
        "type": "boolean"
      },
      "downsamples": {
        "description": "Size factor for each level for each level, downsample is size factor for each level (e.g. (1, 2, 10)).",
        "optional": true,
        "type": "string"
      },
      "dropFirstRowAndColumn": {
        "default": false,
        "description": "Drop first row and column of the source image in order to workaround bug",
        "type": "boolean"
      },
      "levels": {
        "default": 0,
        "description": "Number of levels to generate, levels == 0 means number of levels will be read from wsi file.",
        "type": "integer"
      },
      "seriesDescription": {
        "description": "(0008,103E) [LO] SeriesDescription Dicom tag.",
        "optional": true,
        "type": "string"
      },
      "seriesId": {
        "description": "(0020,000E) [UI] SeriesInstanceUID Dicom tag.",
        "optional": true,
        "type": "string"
      },
      "sparse": {
        "default": false,
        "description": "Use TILED_SPARSE frame organization, by default it's TILED_FULL",
        "type": "boolean"
      },
      "startOn": {
        "default": 0,
        "description": "Level to start generation.",
        "type": "integer"
      },
      "stopOn": {
        "default": -1,
        "description": "Level to stop generation (\"Max Level\" is -1).",
        "type": "integer"
      },
      "studyId": {
        "description": "(0020,000D) [UI] StudyInstanceUID Dicom tag.",
        "optional": true,
        "type": "string"
      },
      "tag": {
        "default": "wsi-to-dicom",
        "description": "The tag to be added on output file upon run completion.",
        "type": "string"
      },
      "threads": {
        "default": -1,
        "description": "number of threads (\"Maximum Threads\" is -1)",
        "type": "integer"
      },
      "tileHeight": {
        "default": 512,
        "description": "Tile height px (power of two preferred).",
        "type": "integer"
      },
      "tileWidth": {
        "default": 512,
        "description": "Tile width px (power of two preferred).",
        "type": "integer"
      }
    },
    "custom": {
      "flywheel": {
        "classification": {
          "function": [
            "Conversion"
          ],
          "modality": [
            "Other"
          ],
          "organ": [
            "Any"
          ],
          "species": [
            "Human"
          ],
          "therapeutic_area": [
            "Any"
          ]
        },
        "suite": "Conversion"
      },
      "gear-builder": {
        "category": "converter",
        "image": "flywheel/wsi-to-dicom:0.2.1_1.0.3"
      }
    },
    "description": "This gear contains a tool that converts whole slide images (WSIs) to DICOM. To read the underlying whole slide images (WSIs), this tool relies on OpenSlide, which supports a variety of file formats.",
    "environment": {
      "FLYWHEEL": "/flywheel/v0",
      "GPG_KEY": "A035C8C19219BA821ECEA86B64E628F8D684696D",
      "LANG": "C.UTF-8",
      "PATH": "/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
      "PWD": "/flywheel/v0",
      "PYTHON_GET_PIP_SHA256": "dfe9fd5c28dc98b5ac17979a953ea550cec37ae1b47a5116007395bfacff2ab9",
      "PYTHON_GET_PIP_URL": "https://github.com/pypa/get-pip/raw/dbf0c85f76fb6e1ab42aa672ffca6f0a675d9ee4/public/get-pip.py",
      "PYTHON_PIP_VERSION": "23.0.1",
      "PYTHON_SETUPTOOLS_VERSION": "65.5.1",
      "PYTHON_VERSION": "3.10.14",
      "VERSION": "1.0.3"
    },
    "inputs": {
      "api-key": {
        "base": "api-key"
      },
      "input_file": {
        "base": "file",
        "description": "Input WSI file, supported by OpenSlide",
        "optional": false
      },
      "jsonFile": {
        "base": "file",
        "description": "DICOM json file with additional tags (See DICOM JSON Model)",
        "optional": true,
        "type": {
          "enum": [
            "source code"
          ]
        }
      }
    },
    "label": "WSI to DICOM",
    "license": "Other",
    "maintainer": "Flywheel <support@flywheel.io>",
    "name": "wsi-to-dicom",
    "source": "https://github.com/GoogleCloudPlatform/wsi-to-dicom-converter",
    "url": "https://gitlab.com/flywheel-io/scientific-solutions/gears/wsi-to-dicom",
    "version": "0.2.1_1.0.3"
  },
  "invocation-schema": {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "properties": {
      "config": {
        "additionalProperties": false,
        "properties": {
          "batch": {
            "default": -1,
            "type": "integer"
          },
          "compression": {
            "default": "jpeg",
            "enum": [
              "jpeg",
              "jpeg2000",
              "raw"
            ],
            "type": "string"
          },
          "debug": {
            "default": false,
            "type": "boolean"
          },
          "downsamples": {
            "type": "string"
          },
          "dropFirstRowAndColumn": {
            "default": false,
            "type": "boolean"
          },
          "levels": {
            "default": 0,
            "type": "integer"
          },
          "seriesDescription": {
            "type": "string"
          },
          "seriesId": {
            "type": "string"
          },
          "sparse": {
            "default": false,
            "type": "boolean"
          },
          "startOn": {
            "default": 0,
            "type": "integer"
          },
          "stopOn": {
            "default": -1,
            "type": "integer"
          },
          "studyId": {
            "type": "string"
          },
          "tag": {
            "default": "wsi-to-dicom",
            "type": "string"
          },
          "threads": {
            "default": -1,
            "type": "integer"
          },
          "tileHeight": {
            "default": 512,
            "type": "integer"
          },
          "tileWidth": {
            "default": 512,
            "type": "integer"
          }
        },
        "required": [
          "batch",
          "compression",
          "debug",
          "dropFirstRowAndColumn",
          "levels",
          "sparse",
          "startOn",
          "stopOn",
          "tag",
          "threads",
          "tileHeight",
          "tileWidth"
        ],
        "type": "object"
      },
      "inputs": {
        "properties": {
          "api-key": {
            "type": "object"
          },
          "input_file": {
            "properties": {},
            "type": "object"
          },
          "jsonFile": {
            "properties": {
              "type": {
                "enum": [
                  "source code"
                ]
              }
            },
            "type": "object"
          }
        },
        "required": [
          "api-key",
          "input_file"
        ],
        "type": "object"
      }
    },
    "required": [
      "config",
      "inputs"
    ],
    "title": "Invocation manifest for WSI to DICOM",
    "type": "object"
  }
}
