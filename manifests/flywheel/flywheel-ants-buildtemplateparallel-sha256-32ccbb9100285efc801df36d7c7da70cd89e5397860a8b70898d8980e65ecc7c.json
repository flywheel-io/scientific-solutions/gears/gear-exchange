{
  "exchange": {
    "git-commit": "6c52031a5e759600e8c3cb60b8901900d3daf2df",
    "rootfs-hash": "sha256:32ccbb9100285efc801df36d7c7da70cd89e5397860a8b70898d8980e65ecc7c",
    "rootfs-url": "docker://us-docker.pkg.dev/flywheel-exchange/gear-exchange/flywheel-ants-buildtemplateparallel:0.1.1_2.3.5"
  },
  "gear": {
    "author": "Flywheel",
    "cite": "A reproducible evaluation of ANTs similarity metric performance in brain image registration: Avants BB, Tustison NJ, Song G, Cook PA, Klein A, Gee JC. Neuroimage, 2011. http://www.ncbi.nlm.nih.gov/pubmed/20851191",
    "command": "poetry run python run.py",
    "config": {
      "Image Dimension": {
        "default": 3,
        "description": "ImageDimension: 2 or 3 (for 2 or 3 dimensional registration of single volume)\nImageDimension: 4 (for template generation of time-series data)",
        "maximum": 4,
        "minimum": 2,
        "type": "integer"
      },
      "Input Glob Pattern": {
        "default": "",
        "description": "Glob pattern (Unix style pathname pattern expansion) that matches filename to be used as inputs. (Default ''). https://en.wikipedia.org/wiki/Glob_(programming).",
        "type": "string"
      },
      "Input Regex": {
        "default": ".*nii\\.gz",
        "description": "Regular expression that matches filenames to be used as inputs. (Default '.*nii\\.gz'). https://en.wikipedia.org/wiki/Regular_expression.",
        "type": "string"
      },
      "Input Tags": {
        "default": "",
        "description": "Tag(s) that matches files to be used as inputs. When multiple tags are specified, they must be comma separated (e.g. T1template,ANTs)",
        "type": "string"
      },
      "Max Iterations": {
        "default": "30x50x30",
        "description": "Max-iterations\nMax-Iterations in form: JxKxL where\n  J = max iterations at coarsest resolution (here, reduce by power of 2^2)\n\tK = middle resolution iterations (here,reduce by power of 2)\n\tL = fine resolution iterations (here, full resolution) !!this level takes much more time per iteration!!\nAdding an extra value before JxKxL (i.e. resulting in IxJxKxL) would add another iteration level.",
        "type": "string"
      },
      "N4 Bias Field Correction": {
        "default": true,
        "description": "N4BiasFieldCorrection of moving image (default True) True == on, False == off. If True, will run N4 before each registration. It is\n          more efficient to run N4BiasFieldCorrection on the input images once, then build a template from the corrected images.",
        "type": "boolean"
      },
      "Output File Prefix": {
        "default": "AntsTemplate",
        "description": "A prefix that is prepended to all output files.",
        "type": "string"
      },
      "Rigid-body Registration": {
        "default": false,
        "description": "Do rigid-body registration of inputs before creating template. Only useful when you do not have an initial template.",
        "type": "boolean"
      },
      "Similarity Metric": {
        "default": "CC",
        "description": "Type of similarity metric used for registration.\nFor intramodal image registration, use:\n  CC = cross-correlation\n  MI = mutual information\n  PR = probability mapping\n  MSQ = mean square difference (Demons-like)\n  SSD = sum of squared differences\nFor intermodal image registration, use:\n  MI = mutual information\n  PR = probability mapping\n",
        "enum": [
          "CC",
          "PR",
          "MI",
          "MSQ"
        ],
        "type": "string"
      },
      "Target Template": {
        "default": "MNI152_T1_1mm.nii.gz",
        "description": "Use this volume as the target of all inputs. When set to None, the script will create an unbiased starting point by averaging all inputs.",
        "enum": [
          "None",
          "MNI152_T1_1mm.nii.gz"
        ],
        "type": "string"
      },
      "Transformation Model": {
        "default": "GR",
        "description": "Type of transformation model used for registration (EL = elastic transformation model, SY = SyN with time, arbitrary number of time points, S2 =  SyN with time, optimized for 2 time points, GR = greedy SyN, EX = exponential, DD = diffeomorphic demons style exponential, mapping, RI = purely rigid, RA = Affine rigid)",
        "enum": [
          "GR",
          "EL",
          "SY",
          "S2",
          "EX",
          "DD",
          "RI",
          "RA"
        ],
        "type": "string"
      },
      "debug": {
        "default": false,
        "description": "Log debug messages",
        "type": "boolean"
      }
    },
    "custom": {
      "flywheel": {
        "classification": {
          "components": [
            "ANTs"
          ],
          "function": [
            "Image Processing - Structural"
          ],
          "keywords": [
            "registration",
            "template"
          ],
          "modality": [
            "MR"
          ],
          "organ": [
            "Brain"
          ],
          "species": [
            "Human"
          ],
          "therapeutic_area": [
            "Neurology"
          ],
          "type": [
            "nifti"
          ]
        },
        "suite": "Image Processing"
      },
      "gear-builder": {
        "category": "analysis",
        "image": "flywheel/ants-buildtemplateparallel:0.1.1_2.3.5"
      }
    },
    "description": "ANTs based gear that run buildtemplateparallel.sh script and generate a template image by co-registering a set of inputs images",
    "environment": {
      "ANTSPATH": "/opt/ants/bin/",
      "FLYWHEEL": "/flywheel/v0",
      "PATH": "/opt/ants/bin/:/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
    },
    "inputs": {
      "api-key": {
        "base": "api-key",
        "read-only": false
      }
    },
    "label": "ANTs Build Template Parallel",
    "license": "MIT",
    "maintainer": "Flywheel <support@flywheel.io>",
    "name": "ants-buildtemplateparallel",
    "source": "https://gitlab.com/flywheel-io/flywheel-apps/ants-buildtemplateparallel",
    "url": "https://gitlab.com/flywheel-io/flywheel-apps/ants-buildtemplateparallel",
    "version": "0.1.1_2.3.5"
  },
  "invocation-schema": {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "properties": {
      "config": {
        "additionalProperties": false,
        "properties": {
          "Image Dimension": {
            "default": 3,
            "maximum": 4,
            "minimum": 2,
            "type": "integer"
          },
          "Input Glob Pattern": {
            "default": "",
            "type": "string"
          },
          "Input Regex": {
            "default": ".*nii\\.gz",
            "type": "string"
          },
          "Input Tags": {
            "default": "",
            "type": "string"
          },
          "Max Iterations": {
            "default": "30x50x30",
            "type": "string"
          },
          "N4 Bias Field Correction": {
            "default": true,
            "type": "boolean"
          },
          "Output File Prefix": {
            "default": "AntsTemplate",
            "type": "string"
          },
          "Rigid-body Registration": {
            "default": false,
            "type": "boolean"
          },
          "Similarity Metric": {
            "default": "CC",
            "enum": [
              "CC",
              "PR",
              "MI",
              "MSQ"
            ],
            "type": "string"
          },
          "Target Template": {
            "default": "MNI152_T1_1mm.nii.gz",
            "enum": [
              "None",
              "MNI152_T1_1mm.nii.gz"
            ],
            "type": "string"
          },
          "Transformation Model": {
            "default": "GR",
            "enum": [
              "GR",
              "EL",
              "SY",
              "S2",
              "EX",
              "DD",
              "RI",
              "RA"
            ],
            "type": "string"
          },
          "debug": {
            "default": false,
            "type": "boolean"
          }
        },
        "required": [
          "Image Dimension",
          "Max Iterations",
          "N4 Bias Field Correction",
          "Similarity Metric",
          "Transformation Model",
          "Output File Prefix",
          "Target Template",
          "Rigid-body Registration",
          "Input Glob Pattern",
          "Input Regex",
          "Input Tags",
          "debug"
        ],
        "type": "object"
      },
      "inputs": {
        "properties": {
          "api-key": {
            "type": "object"
          }
        },
        "required": [
          "api-key"
        ],
        "type": "object"
      }
    },
    "required": [
      "config",
      "inputs"
    ],
    "title": "Invocation manifest for ANTs Build Template Parallel",
    "type": "object"
  }
}
