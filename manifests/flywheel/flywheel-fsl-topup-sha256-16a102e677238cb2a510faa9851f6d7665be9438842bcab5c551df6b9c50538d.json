{
  "exchange": {
    "git-commit": "6c52031a5e759600e8c3cb60b8901900d3daf2df",
    "rootfs-hash": "sha256:16a102e677238cb2a510faa9851f6d7665be9438842bcab5c551df6b9c50538d",
    "rootfs-url": "docker://us-docker.pkg.dev/flywheel-exchange/gear-exchange/flywheel-fsl-topup:0.0.6"
  },
  "gear": {
    "author": "FSL",
    "command": "source /venv/bin/activate && python run.py",
    "config": {
      "QA": {
        "default": true,
        "description": "Save a topup QA image comparing distorted to corrected images",
        "type": "boolean"
      },
      "displacement_field": {
        "default": false,
        "description": "Save displacement fields",
        "type": "boolean"
      },
      "gear-log-level": {
        "default": "INFO",
        "description": "Gear Log verbosity level (ERROR|WARNING|INFO|DEBUG)",
        "enum": [
          "ERROR",
          "WARNING",
          "INFO",
          "DEBUG"
        ],
        "type": "string"
      },
      "jacobian_determinants": {
        "default": false,
        "description": "Save jacobian determinants",
        "type": "boolean"
      },
      "rigid_body_matrix": {
        "default": true,
        "description": "Save rigid body transformation matricies to align volumes",
        "type": "boolean"
      },
      "topup_debug_level": {
        "default": 0,
        "description": "Topup Log verbosity level (0|1|2|3)",
        "enum": [
          0,
          1,
          2,
          3
        ],
        "type": "integer"
      },
      "topup_only": {
        "default": false,
        "description": "Only run topup and get fieldmaps (do no correct images)",
        "type": "boolean"
      },
      "verbose": {
        "default": false,
        "description": "Output verbose information to the log",
        "type": "boolean"
      }
    },
    "custom": {
      "docker-image": "flywheel/fsl-topup:0.0.6_5.0.10",
      "flywheel": {
        "classification": {
          "function": [
            "Image Processing - Functional",
            "Image Processing - Diffusion",
            "Image Processing - Structural"
          ],
          "modality": [
            "MR"
          ],
          "organ": [
            "Brain"
          ],
          "species": [
            "Human",
            "Animal"
          ],
          "therapeutic_area": [
            "Neurology",
            "Psychiatry/Psychology"
          ]
        },
        "suite": "Image Processing"
      },
      "gear-builder": {
        "category": "analysis",
        "image": "flywheel/fsl-topup:0.0.6_5.0.10"
      }
    },
    "description": "Estimates a distortion correction field given one or more pairs of images with opposite PE directions.  LICENSING NOTE: FSL software are owned by Oxford University Innovation and license is required for any commercial applications. For commercial licence please contact fsl@innovation.ox.ac.uk. For academic use, an academic license is required which is available by registering on the FSL website. Any use of the software requires that the user obtain the appropriate license. See https://fsl.fmrib.ox.ac.uk/fsldownloads_registration for more information.",
    "environment": {},
    "inputs": {
      "acquisition_parameters": {
        "base": "file",
        "description": "This parameter specifies a text-file that contains information about how the volumes were acquired.  Row 1 is for image 1, row 2 is for image 2",
        "optional": true,
        "type": {
          "enum": [
            "text",
            ""
          ]
        }
      },
      "apply_to_1": {
        "base": "file",
        "description": "An image to apply the TOPUP correction to using this fieldmap.  Should have the same PE direction as image 1",
        "optional": true,
        "type": {
          "enum": [
            "nifti"
          ]
        }
      },
      "apply_to_2": {
        "base": "file",
        "description": "An image to apply the TOPUP correction to using this fieldmap.  Should have the same PE direction as image 2",
        "optional": true,
        "type": {
          "enum": [
            "nifti"
          ]
        }
      },
      "config_file": {
        "base": "file",
        "description": "A configuration file is a text file containing some or all of the parameters that can be specified for topup. The name of the file should be passed as argument to the --config parameter. It should be an ascii-file with one row for each parameter, and where comments (ignored by topup) are preceeded by a #.",
        "optional": true,
        "type": {
          "enum": [
            "text",
            ""
          ]
        }
      },
      "image_1": {
        "base": "file",
        "description": "First image in a pair of images with opposite phase encoding direction",
        "optional": false,
        "type": {
          "enum": [
            "nifti"
          ]
        }
      },
      "image_2": {
        "base": "file",
        "description": "Second image in a pair of images with opposite phase encoding direction",
        "optional": false,
        "type": {
          "enum": [
            "nifti"
          ]
        }
      }
    },
    "label": "FSL: TOPUP correction for susceptibility induced distortions",
    "license": "MIT",
    "maintainer": "Flywheel <support@flywheel.io>",
    "name": "fsl-topup",
    "source": "https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/topup",
    "url": "https://github.com/flywheel-apps/fsl-topup",
    "version": "0.0.6"
  },
  "invocation-schema": {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "properties": {
      "config": {
        "additionalProperties": false,
        "properties": {
          "QA": {
            "default": true,
            "type": "boolean"
          },
          "displacement_field": {
            "default": false,
            "type": "boolean"
          },
          "gear-log-level": {
            "default": "INFO",
            "enum": [
              "ERROR",
              "WARNING",
              "INFO",
              "DEBUG"
            ],
            "type": "string"
          },
          "jacobian_determinants": {
            "default": false,
            "type": "boolean"
          },
          "rigid_body_matrix": {
            "default": true,
            "type": "boolean"
          },
          "topup_debug_level": {
            "default": 0,
            "enum": [
              0,
              1,
              2,
              3
            ],
            "type": "integer"
          },
          "topup_only": {
            "default": false,
            "type": "boolean"
          },
          "verbose": {
            "default": false,
            "type": "boolean"
          }
        },
        "required": [
          "gear-log-level",
          "topup_only",
          "displacement_field",
          "jacobian_determinants",
          "rigid_body_matrix",
          "verbose",
          "topup_debug_level",
          "QA"
        ],
        "type": "object"
      },
      "inputs": {
        "properties": {
          "acquisition_parameters": {
            "properties": {
              "type": {
                "enum": [
                  "text",
                  ""
                ]
              }
            },
            "type": "object"
          },
          "apply_to_1": {
            "properties": {
              "type": {
                "enum": [
                  "nifti"
                ]
              }
            },
            "type": "object"
          },
          "apply_to_2": {
            "properties": {
              "type": {
                "enum": [
                  "nifti"
                ]
              }
            },
            "type": "object"
          },
          "config_file": {
            "properties": {
              "type": {
                "enum": [
                  "text",
                  ""
                ]
              }
            },
            "type": "object"
          },
          "image_1": {
            "properties": {
              "type": {
                "enum": [
                  "nifti"
                ]
              }
            },
            "type": "object"
          },
          "image_2": {
            "properties": {
              "type": {
                "enum": [
                  "nifti"
                ]
              }
            },
            "type": "object"
          }
        },
        "required": [
          "image_1",
          "image_2"
        ],
        "type": "object"
      }
    },
    "required": [
      "config",
      "inputs"
    ],
    "title": "Invocation manifest for FSL: TOPUP correction for susceptibility induced distortions",
    "type": "object"
  }
}
