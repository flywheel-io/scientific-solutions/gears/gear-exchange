{
  "exchange": {
    "git-commit": "6c52031a5e759600e8c3cb60b8901900d3daf2df",
    "rootfs-hash": "sha256:bf66a8dbb15a1d24761f21ce9474acf35ffa9b0ed425f88d64fb266e3bc8de7f",
    "rootfs-url": "docker://us-docker.pkg.dev/flywheel-exchange/gear-exchange/flywheel-fsl-stats:0.1.5_5.0.9"
  },
  "gear": {
    "author": "Analysis Group, FMRIB, Oxford, UK",
    "cite": "",
    "command": "poetry run python3 run.py",
    "config": {
      "Absolute values?": {
        "description": "use absolute values of all image intensities",
        "id": "a",
        "optional": true,
        "type": "boolean"
      },
      "Center of gravity (mm)": {
        "description": "output centre-of-gravity (cog) in mm coordinates",
        "id": "c",
        "optional": true,
        "type": "boolean"
      },
      "Center of gravity (voxels)": {
        "description": "output centre-of-gravity (cog) in voxel coordinates",
        "id": "C",
        "optional": true,
        "type": "boolean"
      },
      "Entropy": {
        "description": "output mean entropy ; mean(-i*ln(i))",
        "id": "e",
        "optional": true,
        "type": "boolean"
      },
      "Entropy (nonzero)": {
        "description": "output mean entropy (of nonzero voxels)",
        "id": "E",
        "optional": true,
        "type": "boolean"
      },
      "Lower threshold": {
        "description": "set lower threshold",
        "id": "l",
        "optional": true,
        "type": "number"
      },
      "Max voxel coords": {
        "description": "output co-ordinates of maximum voxel",
        "id": "x",
        "optional": true,
        "type": "boolean"
      },
      "Mean intensity": {
        "default": true,
        "description": "output mean",
        "id": "m",
        "type": "boolean"
      },
      "Mean intensity (nonzero)": {
        "default": true,
        "description": "output mean (for nonzero voxels)",
        "id": "M",
        "type": "boolean"
      },
      "Min voxel coords": {
        "description": "output co-ordinates of minimum voxel",
        "id": "X",
        "optional": true,
        "type": "boolean"
      },
      "Min/max": {
        "description": "output <min intensity> <max intensity>",
        "id": "R",
        "optional": true,
        "type": "boolean"
      },
      "NaN/Inf as zero?": {
        "description": "treat NaN or Inf as zero for subsequent stats",
        "id": "n",
        "optional": true,
        "type": "boolean"
      },
      "Percentile": {
        "description": "output nth percentile (n between 0 and 100)",
        "id": "p",
        "maximum": 100,
        "minimum": 0,
        "optional": true,
        "type": "number"
      },
      "Percentile (nonzero)": {
        "description": "output nth percentile (for nonzero voxels)",
        "id": "P",
        "maximum": 100,
        "minimum": 0,
        "optional": true,
        "type": "number"
      },
      "ROI stats": {
        "description": "output smallest ROI <xmin> <xsize> <ymin> <ysize> <zmin> <zsize> <tmin> <tsize>\n containing nonzero voxels",
        "id": "w",
        "optional": true,
        "type": "boolean"
      },
      "Robust min/max": {
        "description": "output <robust min intensity> <robust max intensity>",
        "id": "r",
        "optional": true,
        "type": "boolean"
      },
      "Split by timepoint": {
        "description": "Complete stats for each volume",
        "id": "t",
        "optional": true,
        "type": "boolean"
      },
      "Stdev": {
        "description": "output standard deviation",
        "id": "s",
        "optional": true,
        "type": "boolean"
      },
      "Stdev (nonzero)": {
        "description": "output standard deviation (for nonzero voxels)",
        "id": "S",
        "optional": true,
        "type": "boolean"
      },
      "Upper threshold": {
        "description": "set upper threshold",
        "id": "u",
        "optional": true,
        "type": "number"
      },
      "Volume": {
        "description": "output <voxels> <volume>",
        "id": "v",
        "optional": true,
        "type": "boolean"
      },
      "Volume (nonzero)": {
        "description": "output <voxels> <volume> (for nonzero voxels)",
        "id": "V",
        "optional": true,
        "type": "boolean"
      },
      "Windowed nbins for histogram": {
        "description": "nbins, min, and max for output a histogram (for the thresholded/masked voxels only)\nwith nbins and histogram limits of min and max",
        "enum": [
          ""
        ],
        "id": "H",
        "optional": true,
        "type": "string"
      },
      "debug": {
        "default": false,
        "description": "Log debug messages",
        "type": "boolean"
      },
      "nbins for histogram": {
        "description": "nbins for output a histogram (for the thresholded/masked voxels only) with nbins",
        "id": "h",
        "maximum": 100,
        "minimum": 1,
        "optional": true,
        "type": "number"
      }
    },
    "custom": {
      "flywheel": {
        "classification": {
          "components": [
            "FSL"
          ],
          "function": [
            "Image Processing - Other"
          ],
          "keywords": [
            "Neuroimaging",
            "Summary",
            "Statistics"
          ],
          "modality": [
            "Any"
          ],
          "organ": [
            "Any"
          ],
          "species": [
            "Human",
            "Animal"
          ],
          "therapeutic_area": [
            "Any"
          ],
          "type": [
            "NIFTI"
          ]
        },
        "suite": "Image Processing"
      },
      "gear-builder": {
        "category": "analysis",
        "image": "flywheel/fsl-stats:0.1.5_5.0.9"
      }
    },
    "description": "This gear returns statistical output for a given NIFTI image.  LICENSING NOTE: FSL software are owned by Oxford University Innovation and license is required for any commercial applications. For commercial licence please contact fsl@innovation.ox.ac.uk. For academic use, an academic license is required which is available by registering on the FSL website. Any use of the software requires that the user obtain the appropriate license. See https://fsl.fmrib.ox.ac.uk/fsldownloads_registration for more information.",
    "environment": {
      "FLYWHEEL": "/flywheel/v0",
      "LD_LIBRARY_PATH": "/usr/lib/fsl/5.0",
      "PATH": "/usr/lib/fsl/5.0:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
      "POSSUMDIR": "/usr/share/fsl/5.0"
    },
    "inputs": {
      "difference_image": {
        "base": "file",
        "description": "(-d): take the difference between the base image and the image specified here",
        "optional": true,
        "type": {
          "enum": [
            "nifti"
          ]
        }
      },
      "input_image": {
        "base": "file",
        "description": "Main image for which stats will be generated",
        "type": {
          "enum": [
            "nifti"
          ]
        }
      },
      "mask_image": {
        "base": "file",
        "description": "(-k): use the specified image (filename) for masking - overrides lower and upper thresholds",
        "optional": true,
        "type": {
          "enum": [
            "nifti"
          ]
        }
      }
    },
    "label": "FSL fslstats",
    "license": "MIT",
    "maintainer": "Flywheel <support@flywheel.io>",
    "name": "fsl-stats",
    "source": "https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/Fslutils",
    "url": "https://gitlab.com/flywheel-io/flywheel-apps/fsl-stats",
    "version": "0.1.5_5.0.9"
  },
  "invocation-schema": {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "properties": {
      "config": {
        "additionalProperties": false,
        "properties": {
          "Absolute values?": {
            "id": "a",
            "type": "boolean"
          },
          "Center of gravity (mm)": {
            "id": "c",
            "type": "boolean"
          },
          "Center of gravity (voxels)": {
            "id": "C",
            "type": "boolean"
          },
          "Entropy": {
            "id": "e",
            "type": "boolean"
          },
          "Entropy (nonzero)": {
            "id": "E",
            "type": "boolean"
          },
          "Lower threshold": {
            "id": "l",
            "type": "number"
          },
          "Max voxel coords": {
            "id": "x",
            "type": "boolean"
          },
          "Mean intensity": {
            "default": true,
            "id": "m",
            "type": "boolean"
          },
          "Mean intensity (nonzero)": {
            "default": true,
            "id": "M",
            "type": "boolean"
          },
          "Min voxel coords": {
            "id": "X",
            "type": "boolean"
          },
          "Min/max": {
            "id": "R",
            "type": "boolean"
          },
          "NaN/Inf as zero?": {
            "id": "n",
            "type": "boolean"
          },
          "Percentile": {
            "id": "p",
            "maximum": 100,
            "minimum": 0,
            "type": "number"
          },
          "Percentile (nonzero)": {
            "id": "P",
            "maximum": 100,
            "minimum": 0,
            "type": "number"
          },
          "ROI stats": {
            "id": "w",
            "type": "boolean"
          },
          "Robust min/max": {
            "id": "r",
            "type": "boolean"
          },
          "Split by timepoint": {
            "id": "t",
            "type": "boolean"
          },
          "Stdev": {
            "id": "s",
            "type": "boolean"
          },
          "Stdev (nonzero)": {
            "id": "S",
            "type": "boolean"
          },
          "Upper threshold": {
            "id": "u",
            "type": "number"
          },
          "Volume": {
            "id": "v",
            "type": "boolean"
          },
          "Volume (nonzero)": {
            "id": "V",
            "type": "boolean"
          },
          "Windowed nbins for histogram": {
            "enum": [
              ""
            ],
            "id": "H",
            "type": "string"
          },
          "debug": {
            "default": false,
            "type": "boolean"
          },
          "nbins for histogram": {
            "id": "h",
            "maximum": 100,
            "minimum": 1,
            "type": "number"
          }
        },
        "required": [
          "debug",
          "Mean intensity",
          "Mean intensity (nonzero)"
        ],
        "type": "object"
      },
      "inputs": {
        "properties": {
          "difference_image": {
            "properties": {
              "type": {
                "enum": [
                  "nifti"
                ]
              }
            },
            "type": "object"
          },
          "input_image": {
            "properties": {
              "type": {
                "enum": [
                  "nifti"
                ]
              }
            },
            "type": "object"
          },
          "mask_image": {
            "properties": {
              "type": {
                "enum": [
                  "nifti"
                ]
              }
            },
            "type": "object"
          }
        },
        "required": [
          "input_image"
        ],
        "type": "object"
      }
    },
    "required": [
      "config",
      "inputs"
    ],
    "title": "Invocation manifest for FSL fslstats",
    "type": "object"
  }
}
