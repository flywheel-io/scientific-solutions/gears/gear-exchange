{
  "exchange": {
    "git-commit": "8bd1baba85e25dcac347204bd1e93ec8ede76501",
    "rootfs-hash": "sha256:0e24773d61fbad62007308f0f22be7c7d3a61ef0632ec3268b594449bea29946",
    "rootfs-url": "docker://us-docker.pkg.dev/flywheel-exchange/gear-exchange/flywheel-curate-bids:2.2.8_1.2.22"
  },
  "gear": {
    "author": "Flywheel <support@flywheel.io>",
    "command": "/flywheel/v0/run.py",
    "config": {
      "base_template": {
        "default": "reproin",
        "description": "Which base template to use. Options: reproin or bids-v1 (for backwards compatibility).  This setting is only used if no curation template is supplied as an input to the gear.  A curation template given as an input file can extend or completely override a base template and a setting in the input template determines which base template to extend, no matter how this configuration is set.",
        "enum": [
          "bids-v1",
          "reproin"
        ],
        "type": "string"
      },
      "intendedfor_regexes": {
        "default": "",
        "description": "A list of pairs of regular expressions.  The first regex matches the field map acquisition label and the second matches the IntendedFor BIDS path.  Initial processing sets the list of IntendedFor paths to all files in the different 'anat', 'func', and 'dwi' BIDS folders.  These regex pairs should be used to filter that list so that only the appropriate IntendedFor paths remain.",
        "type": "string"
      },
      "reset": {
        "default": false,
        "description": "Remove all BIDS info from files before curating",
        "type": "boolean"
      },
      "save_sidecar_as_metadata": {
        "default": "auto",
        "description": "Instead of assuming sidecar data is in actual json sidecar, put it in file.info metadata.  Default is 'auto' (detect if previous BIDS metadata exists on project.  Other choices are 'yes' (old way) or 'no' (best practice).",
        "enum": [
          "auto",
          "yes",
          "no"
        ],
        "type": "string"
      },
      "use_or_save_config": {
        "default": "",
        "description": "This option sets the configuration and input file that will be used on subsequent runs of this gear.  If you choose 'Save Config File', a configuration file will be attached to the project called 'curate-bids-<run level>-config.json' where <run level> is 'project', 'subject' or 'session' depending on how you ran the gear.  If you choose 'Ignore Config File', that file will be ignored and the currently set configuration will be used.  The default, leaving this option blank, is to use the saved configuration file if it is present or to use the current configuration if not.  For example, using the 'Save Config File' option one time and providing a curation template as input, that will become the default for future runs so you can just press the 'Run Gear' button and don't have to adjust anything.  To turn this feature off, choose 'Disable Config File' which will disable the configuration file and quit without curating.",
        "enum": [
          "",
          "Save Config File",
          "Ignore Config File",
          "Disable Config File"
        ],
        "type": "string"
      },
      "verbosity": {
        "default": "INFO",
        "description": "Set log level to INFO or DEBUG.",
        "enum": [
          "INFO",
          "DEBUG"
        ],
        "type": "string"
      }
    },
    "custom": {
      "flywheel": {
        "classification": {
          "function": [
            "Curation"
          ],
          "modality": [
            "MR",
            "MEG",
            "EEG"
          ],
          "organ": [
            "Brain"
          ],
          "species": [
            "Phantom",
            "Human",
            "Animal",
            "Other"
          ],
          "therapeutic_area": [
            "Neurology",
            "Psychiatry/Psychology"
          ]
        },
        "show-job": true,
        "suite": "Curation"
      },
      "gear-builder": {
        "category": "analysis",
        "image": "flywheel/curate-bids:2.2.8_1.2.22"
      }
    },
    "description": "Use this gear to initialize BIDS filenames and attributes on all files within a given project.",
    "environment": {
      "FLYWHEEL": "/flywheel/v0",
      "GPG_KEY": "A035C8C19219BA821ECEA86B64E628F8D684696D",
      "LANG": "C.UTF-8",
      "PATH": "/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
      "PWD": "/flywheel/v0",
      "PYTHON_GET_PIP_SHA256": "bc37786ec99618416cc0a0ca32833da447f4d91ab51d2c138dd15b7af21e8e9a",
      "PYTHON_GET_PIP_URL": "https://github.com/pypa/get-pip/raw/def4aec84b261b939137dd1c69eff0aabb4a7bf4/public/get-pip.py",
      "PYTHON_PIP_VERSION": "24.0",
      "PYTHON_SETUPTOOLS_VERSION": "65.5.1",
      "PYTHON_VERSION": "3.11.10"
    },
    "inputs": {
      "api-key": {
        "base": "api-key",
        "read-only": false
      },
      "template": {
        "base": "file",
        "description": "A project curation JSON template.  This can extend or override the base template chosen in the configuration.",
        "optional": true,
        "type": {
          "enum": [
            "json"
          ]
        }
      }
    },
    "label": "BIDS Curation",
    "license": "MIT",
    "maintainer": "Flywheel <support@flywheel.io>",
    "name": "curate-bids",
    "source": "https://bids.neuroimaging.io/",
    "url": "https://gitlab.com/flywheel-io/scientific-solutions/gears/curate-bids",
    "version": "2.2.8_1.2.22"
  },
  "invocation-schema": {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "properties": {
      "config": {
        "additionalProperties": false,
        "properties": {
          "base_template": {
            "default": "reproin",
            "enum": [
              "bids-v1",
              "reproin"
            ],
            "type": "string"
          },
          "intendedfor_regexes": {
            "default": "",
            "type": "string"
          },
          "reset": {
            "default": false,
            "type": "boolean"
          },
          "save_sidecar_as_metadata": {
            "default": "auto",
            "enum": [
              "auto",
              "yes",
              "no"
            ],
            "type": "string"
          },
          "use_or_save_config": {
            "default": "",
            "enum": [
              "",
              "Save Config File",
              "Ignore Config File",
              "Disable Config File"
            ],
            "type": "string"
          },
          "verbosity": {
            "default": "INFO",
            "enum": [
              "INFO",
              "DEBUG"
            ],
            "type": "string"
          }
        },
        "required": [
          "base_template",
          "intendedfor_regexes",
          "reset",
          "save_sidecar_as_metadata",
          "use_or_save_config",
          "verbosity"
        ],
        "type": "object"
      },
      "inputs": {
        "properties": {
          "api-key": {
            "type": "object"
          },
          "template": {
            "properties": {
              "type": {
                "enum": [
                  "json"
                ]
              }
            },
            "type": "object"
          }
        },
        "required": [
          "api-key"
        ],
        "type": "object"
      }
    },
    "required": [
      "config",
      "inputs"
    ],
    "title": "Invocation manifest for BIDS Curation",
    "type": "object"
  }
}
