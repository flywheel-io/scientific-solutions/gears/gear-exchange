{
  "exchange": {
    "git-commit": "79170132e194dfb8bf9793be0e38c55548e3ec2f",
    "rootfs-hash": "sha256:6ea6cb38e0be200f5486dccda5a81aa78e9154948223eb5582446c2785779f44",
    "rootfs-url": "docker://us-docker.pkg.dev/flywheel-exchange/gear-exchange/flywheel-freesurferator:0.2.1_7.4.1"
  },
  "gear": {
    "author": "Flywheel",
    "cite": "For citation information, please visit: https://www.biorxiv.org/content/10.1101/2022.03.17.484761v1",
    "command": "python run.py",
    "config": {
      "aparc2009": {
        "default": false,
        "description": "separate the aparc.a2009 from freesurfer to individual images for each segment",
        "type": "boolean"
      },
      "brainstem_structures": {
        "default": false,
        "description": "Generate automated segmentation of four different brainstem structures from the input T1 scan: medulla oblongata, pons, midbrain and superior cerebellar peduncle (SCP). We use a Bayesian segmentation algorithm that relies on a probabilistic atlas of the brainstem (and neighboring brain structures) built upon manual delineations of the structures on interest in 49 scans (10 for the brainstem structures, 39 for the surrounding structures). The delineation protocol for the brainstem was designed by Dr. Adam Boxer and his team at the UCSF Memory and Aging Center, and is described in the paper. Choosing this option will write <subject_id>_BrainstemStructures.csv to the final results. See: https://surfer.nmr.mgh.harvard.edu/fswiki/BrainstemSubstructures for more info. (Default=false)",
        "type": "boolean"
      },
      "cerebellum": {
        "default": false,
        "description": "bring CerebellumParcellation-Bucker2011 (17Networks LooseMask) into native space, and generate separate image files for each volume",
        "type": "boolean"
      },
      "force_ants": {
        "default": false,
        "description": "Ants will be automatically done depending on what segmentations are asked. If this options is set to true, ANTs will be run regardless of those options. Default=false",
        "type": "boolean"
      },
      "freesurfer_license_key": {
        "description": "Text from license file generated during FreeSurfer registration. Entries should be space separated. Usually this should be added at the project level and it will be read from there.",
        "optional": true,
        "type": "string"
      },
      "hcp": {
        "default": false,
        "description": "bring MNI_Glasser_HCP into native space, and generate separate image files for each volume",
        "type": "boolean"
      },
      "hippocampal_subfields": {
        "default": false,
        "description": "Generates an automated segmentation of the hippocampal subfields based on a statistical atlas built primarily upon ultra-high resolution (~0.1 mm isotropic) ex vivo MRI data. Choosing this option will write <subject_id>_HippocampalSubfields.csv to the final results. See: https://surfer.nmr.mgh.harvard.edu/fswiki/HippocampalSubfields for more info. (Default=false)",
        "type": "boolean"
      },
      "mni_rois": {
        "default": false,
        "description": "Bring MNI ROIs such as MNI_JHU_tracts_ROIs, custom created and other ROIs into native space. If no mniroizip input is passed and this is set to True, it will use the default Mori ROIs, CC ROIs and some other (such as eyes or the mOTS and pOTS VWFAs). Download the zip from the Freesurferator repo or check the readme for more information. If this is set to True and a mniroizip input is passed, then both will be added and converted to subject's space. If it is set to False and mniroizip is passed only those MNI ROIs will be converted.",
        "type": "boolean"
      },
      "neuropythy_analysis": {
        "default": false,
        "description": "Perform a neuropythy analysis. See: https://github.com/noahbenson/neuropythy for more info. (Default=false)",
        "type": "boolean"
      },
      "reconall_options": {
        "default": "-all -qcache",
        "description": "Command line options to the recon-all algorithm. (Default='-all -qcache'). By default we enable '-all' and '-qcache'. '-all' runs the entire pipeline and '-qcache' will resample data onto the average subject (called fsaverage) and smooth it at various FWHM (full-width/half-max) values, usually 0, 5, 10, 15, 20, and 25mm, which can speed later processing. Note that modification of these options may result in failure if the options are not recognized. Note: if the optional file control_points are included, then it will write it in the tmp/ folder and it will re-reun it with the options set up in this field. For example, if control points are added for intensity correction, then -autorecon2-cp and -autorecon3 will be expected here instead of -all.",
        "type": "string"
      },
      "rois_in_output": {
        "default": false,
        "description": "Depending on the selection there can be hundreds of ROIs, therefore they will be all zipped in the fs.zip archive in the /ROIs folder. In some cases (few ROIs, debugging) it might be of interest having the ROIs in the output. Default is false.",
        "type": "boolean"
      },
      "run_gtmseg": {
        "default": false,
        "description": "Run gtmseg, it is a step required for PETsurfer. By default, is false.",
        "type": "boolean"
      },
      "subject_id": {
        "default": "Sxxx",
        "description": "Desired subject ID. Any spaces in the subject_id will be replaced with underscores and will be used to name the resulting FreeSurfer output directory. NOTE: If using a previous Gear output as input the subject code will be parsed from the input archive, however it should still be provided here for good measure.",
        "type": "string"
      },
      "thalamic_nuclei": {
        "default": false,
        "description": "Generate parcellation of the thalamus into 25 different nuclei, using a probabilistic atlas built with histological data. The parcellation is based on structural MRI, either the main T1 scan processed through recon-all, or an additional scan of a different modality, which potentially shows better contrast between the nuclei. Choosing this option will write <subject_id>_thalamic-nuclei.lh.v10.T1.csv and <subject_id>_thalamic-nuclei.rh.v10.T1.stats.csv to the final results. See: https://surfer.nmr.mgh.harvard.edu/fswiki/ThalamicNuclei for more info. (Default=false)",
        "type": "boolean"
      }
    },
    "custom": {
      "flywheel": {
        "classification": {
          "function": [
            "Image Processing - Structural"
          ],
          "modality": [
            "MR"
          ],
          "organ": [
            "Brain"
          ],
          "species": [
            "Human"
          ],
          "therapeutic_area": [
            "Psychiatry/Psychology",
            "Neurology"
          ]
        },
        "show-job": true,
        "suite": "Image Processing - Structural"
      },
      "gear-builder": {
        "category": "analysis",
        "image": "flywheel/freesurferator:0.2.1_7.4.1"
      }
    },
    "description": "This gear takes either an existing FS zipped run, or an anatomical NIfTI file and performs all of the FreeSurfer cortical reconstruction process. Outputs are provided in a zip file and include the entire output directory tree from Recon-All. Configuration options exist for setting the subject ID and for converting outputs to NIfTI, OBJ, and CSV. FreeSurfer is a software package for the analysis and visualization of structural and functional neuroimaging data from cross-sectional or longitudinal studies. It is developed by the Laboratory for Computational Neuroimaging at the Athinoula A. Martinos Center for Biomedical Imaging. Please see https://surfer.nmr.mgh.harvard.edu/fswiki/FreeSurferSoftwareLicense for license information. This gear includes a bunch of atlases included (cerebellum, HCP, Neuropythy... and depending on the configuration options it will create many ROIs in subject space that can later be incorporated to other neuroimaging tools (we use it extensively with DWI data, namely RTP-pipeline",
    "environment": {
      "ANTSPATH": "/opt/ants/bin/",
      "ANTS_VERSION": "v2.4.2",
      "DISPLAY": ":50.0",
      "FLYWHEEL": "/flywheel/v0",
      "FMRI_ANALYSIS_DIR": "/opt/freesurfer/fsfast",
      "FREESURFER_HOME": "/opt/freesurfer",
      "FREESURFER_VERSION": "7.4.1",
      "FSFAST_HOME": "/opt/freesurfer/fsfast",
      "FSL_OUTPUT_FORMAT": "nii.gz",
      "FS_LICENSE": "/flywheel/v0/work/license.txt",
      "FS_OVERRIDE": "0",
      "GPG_KEY": "E3FF2839C048B25C084DEBE9B26995E310250568",
      "HOME": "/root",
      "HOSTNAME": "8adec71deed1",
      "LANG": "C.UTF-8",
      "LD_LIBRARY_PATH": "/opt/ants/lib:",
      "LOCAL_DIR": "/opt/freesurfer/local",
      "MCR_CACHE_DIR": "/flywheel/v0/work/.mcrCache9.7",
      "MCR_CACHE_ROOT": "/flywheel/v0/work",
      "MINC_BIN_DIR": "/opt/freesurfer/mni/bin",
      "MINC_LIB_DIR": "/opt/freesurfer/mni/lib",
      "MNI_DATAPATH": "/opt/freesurfer/mni/data",
      "MNI_DIR": "/opt/freesurfer/mni",
      "MNI_PERL5LIB": "/opt/freesurfer/mni/lib/perl5/5.8.5",
      "OS": "Linux",
      "PATH": "/opt/freesurfer/bin:/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/opt/freesurfer/fsfast/bin:/opt/freesurfer/tktools:/opt/freesurfer/mni/bin:/sbin:/bin:/opt/ants/bin",
      "PERL5LIB": "/opt/freesurfer/mni/lib/perl5/5.8.5",
      "PWD": "/flywheel/v0",
      "PYTHON_GET_PIP_SHA256": "1e501cf004eac1b7eb1f97266d28f995ae835d30250bec7f8850562703067dc6",
      "PYTHON_GET_PIP_URL": "https://github.com/pypa/get-pip/raw/66030fa03382b4914d4c4d0896961a0bdeeeb274/public/get-pip.py",
      "PYTHON_PIP_VERSION": "22.0.4",
      "PYTHON_SETUPTOOLS_VERSION": "58.1.0",
      "PYTHON_VERSION": "3.9.15",
      "QT_QPA_PLATFORM": "xcb",
      "SUBJECTS_DIR": "/flywheel/v0/work",
      "TERM": "xterm",
      "WORKDIR": "/flywheel/v0",
      "XAPPLRESDIR": "/opt/freesurfer/MCRv97/X11/app-defaults"
    },
    "inputs": {
      "anat": {
        "base": "file",
        "description": "Anatomical NIfTI file.",
        "optional": false,
        "type": {
          "enum": [
            "nifti"
          ]
        }
      },
      "annotfile": {
        "base": "file",
        "description": "zip with annot files in fsaverage space. Create surface ROIs in fsaverage and save them as annots and them zip them, the system will create cortical volumetric independent ROIs in subject space",
        "optional": true,
        "type": {
          "enum": [
            "zip"
          ]
        }
      },
      "api-key": {
        "base": "api-key",
        "read-only": true
      },
      "control_points": {
        "base": "file",
        "description": "Text file with the control points created in Freeview. You can find them in the tmp/ folder once you create them in Freeview. If there is a control points file and it is correct, Freesurfer will use them. Be careful to add the proper recon-all call. The file name needs to be named control.dat, and you need to copy it from the tmp/ folder, if it is not the case, the gear will fail.",
        "optional": true
      },
      "freesurfer_license_file": {
        "base": "file",
        "description": "FreeSurfer license file, provided during registration with FreeSurfer. This file will be copied to the $FSHOME directory and used during execution of the Gear. It is optional because it will be only required if the Freesurfer option is selected for 'coreg_method', if the license file is not there, the gear will fail.",
        "optional": true
      },
      "mniroizip": {
        "base": "file",
        "description": "ZIP file with ROIs in MNI space. If this zip exists (no folder names, all ROIs in base folder after unziping), the ROIs will be converted to individual subject space. The ROIs need to be in FSL's 1mm isotropic MNI template space. Please check with mri_info (Freesurfer) or mrinfo (MRtrix) or an equivalent software that your ROI and the template are exactly in the same space. The ROIs should be composed of 0-s and 1-s. For downloading an example an several ROIs, please check https://osf.io/download/m6b7r",
        "optional": true,
        "type": {
          "enum": [
            "zip",
            "nifti"
          ]
        }
      },
      "pre_fs": {
        "base": "file",
        "description": "ZIP file with a full freesurfer run",
        "optional": true,
        "type": {
          "enum": [
            "zip"
          ]
        }
      },
      "t1w_anatomical_2": {
        "base": "file",
        "description": "Additional anatomical NIfTI file",
        "optional": true,
        "type": {
          "enum": [
            "nifti"
          ]
        }
      },
      "t1w_anatomical_3": {
        "base": "file",
        "description": "Additional anatomical NIfTI file",
        "optional": true,
        "type": {
          "enum": [
            "nifti"
          ]
        }
      },
      "t1w_anatomical_4": {
        "base": "file",
        "description": "Additional anatomical NIfTI file",
        "optional": true,
        "type": {
          "enum": [
            "nifti"
          ]
        }
      },
      "t1w_anatomical_5": {
        "base": "file",
        "description": "Additional anatomical NIfTI file",
        "optional": true,
        "type": {
          "enum": [
            "nifti"
          ]
        }
      },
      "t2w_anatomical": {
        "base": "file",
        "description": "T2w Anatomical NIfTI file",
        "optional": true,
        "type": {
          "enum": [
            "nifti"
          ]
        }
      }
    },
    "label": "freesurferator: create anatomical ROIs for DWI, PET or fMRI in subject space.",
    "license": "Other",
    "maintainer": "Flywheel <support@flywheel.io>",
    "name": "freesurferator",
    "source": "https://gitlab.com/flywheel-io/scientific-solutions/gears/freesurferator.git",
    "url": "https://gitlab.com/flywheel-io/scientific-solutions/gears/freesurferator.git",
    "version": "0.2.1_7.4.1"
  },
  "invocation-schema": {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "properties": {
      "config": {
        "additionalProperties": false,
        "properties": {
          "aparc2009": {
            "default": false,
            "type": "boolean"
          },
          "brainstem_structures": {
            "default": false,
            "type": "boolean"
          },
          "cerebellum": {
            "default": false,
            "type": "boolean"
          },
          "force_ants": {
            "default": false,
            "type": "boolean"
          },
          "freesurfer_license_key": {
            "type": "string"
          },
          "hcp": {
            "default": false,
            "type": "boolean"
          },
          "hippocampal_subfields": {
            "default": false,
            "type": "boolean"
          },
          "mni_rois": {
            "default": false,
            "type": "boolean"
          },
          "neuropythy_analysis": {
            "default": false,
            "type": "boolean"
          },
          "reconall_options": {
            "default": "-all -qcache",
            "type": "string"
          },
          "rois_in_output": {
            "default": false,
            "type": "boolean"
          },
          "run_gtmseg": {
            "default": false,
            "type": "boolean"
          },
          "subject_id": {
            "default": "Sxxx",
            "type": "string"
          },
          "thalamic_nuclei": {
            "default": false,
            "type": "boolean"
          }
        },
        "required": [
          "aparc2009",
          "brainstem_structures",
          "cerebellum",
          "force_ants",
          "hcp",
          "hippocampal_subfields",
          "mni_rois",
          "neuropythy_analysis",
          "reconall_options",
          "rois_in_output",
          "run_gtmseg",
          "subject_id",
          "thalamic_nuclei"
        ],
        "type": "object"
      },
      "inputs": {
        "properties": {
          "anat": {
            "properties": {
              "type": {
                "enum": [
                  "nifti"
                ]
              }
            },
            "type": "object"
          },
          "annotfile": {
            "properties": {
              "type": {
                "enum": [
                  "zip"
                ]
              }
            },
            "type": "object"
          },
          "api-key": {
            "type": "object"
          },
          "control_points": {
            "properties": {},
            "type": "object"
          },
          "freesurfer_license_file": {
            "properties": {},
            "type": "object"
          },
          "mniroizip": {
            "properties": {
              "type": {
                "enum": [
                  "zip",
                  "nifti"
                ]
              }
            },
            "type": "object"
          },
          "pre_fs": {
            "properties": {
              "type": {
                "enum": [
                  "zip"
                ]
              }
            },
            "type": "object"
          },
          "t1w_anatomical_2": {
            "properties": {
              "type": {
                "enum": [
                  "nifti"
                ]
              }
            },
            "type": "object"
          },
          "t1w_anatomical_3": {
            "properties": {
              "type": {
                "enum": [
                  "nifti"
                ]
              }
            },
            "type": "object"
          },
          "t1w_anatomical_4": {
            "properties": {
              "type": {
                "enum": [
                  "nifti"
                ]
              }
            },
            "type": "object"
          },
          "t1w_anatomical_5": {
            "properties": {
              "type": {
                "enum": [
                  "nifti"
                ]
              }
            },
            "type": "object"
          },
          "t2w_anatomical": {
            "properties": {
              "type": {
                "enum": [
                  "nifti"
                ]
              }
            },
            "type": "object"
          }
        },
        "required": [
          "anat",
          "api-key"
        ],
        "type": "object"
      }
    },
    "required": [
      "config",
      "inputs"
    ],
    "title": "Invocation manifest for freesurferator: create anatomical ROIs for DWI, PET or fMRI in subject space.",
    "type": "object"
  }
}
