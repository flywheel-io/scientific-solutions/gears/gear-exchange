{
  "exchange": {
    "git-commit": "bd5fd0c8265e161fcdbbd0f41445ce148b6c9213",
    "rootfs-hash": "sha256:f49a0e965f0cf8b4a173e05ffff54e4c06ad26530505dacd514baffcbfe5e61e",
    "rootfs-url": "docker://us-docker.pkg.dev/flywheel-exchange/gear-exchange/flywheel-deid-export:1.7.0"
  },
  "gear": {
    "author": "Flywheel, Inc.",
    "command": "python /flywheel/v0/run.py",
    "config": {
      "debug": {
        "default": false,
        "description": "Enable debug log if checked",
        "type": "boolean"
      },
      "overwrite_files": {
        "default": "Skip",
        "description": "Overwrite strategy when a file to be exported exists in destination containers. Options are Skip= Do not do anything if file exists at destination, Cleanup= Delete and upload a new file if file exists at destination (Error will be raised for smart copied project), Cleanup_force= Delete (force delete for smart copies project) if the file exists at destination and upload a new file , Replace= Upload a new version of the file if file exists at destination",
        "enum": [
          "Skip",
          "Cleanup",
          "Cleanup_force",
          "Replace"
        ],
        "type": "string"
      },
      "private_key": {
        "description": "Asymmetric decryption: Path to private key pem file, formatted as '<group>/<project>/files/<filename>'.",
        "optional": true,
        "type": "string"
      },
      "project_path": {
        "description": "The resolver path of the destination project (E.g., 'flywheel/test').",
        "optional": false,
        "type": "string"
      },
      "public_key": {
        "description": "Asymmetric encryption: Path to public key pem file(s), formatted as '<group>/<project>/files/<filename>'. Separate multiple key files with ', '.",
        "optional": true,
        "type": "string"
      },
      "secret_key": {
        "description": "Symmetric encryption/decryption: Path to secret key txt file, formatted as '<group>/<project>/files/<filename>'.",
        "optional": true,
        "type": "string"
      }
    },
    "custom": {
      "flywheel": {
        "classification": {
          "function": [
            "Curation"
          ],
          "modality": [
            "Any"
          ],
          "organ": [
            "Any"
          ],
          "species": [
            "Phantom",
            "Human",
            "Animal",
            "Other"
          ],
          "therapeutic_area": [
            "Any"
          ]
        },
        "suite": "Curation"
      },
      "gear-builder": {
        "category": "analysis",
        "image": "flywheel/deid-export:1.7.0"
      }
    },
    "description": "Profile-based anonymization and export of files within a project. Files within the source project will be anonymized (according to a required template) and exported to a specified project. Output is a csv file reporting the status of all exported items.",
    "environment": {
      "BUILD_TIME": "2024-11-04T15:21:03Z",
      "CI_JOB_URL": "https://gitlab.com/flywheel-io/tools/img/python/-/jobs/8263113721",
      "COMMIT_REF": "main",
      "COMMIT_SHA": "1b5605d4",
      "EDITOR": "micro",
      "EGET_BIN": "/opt/bin",
      "FLYWHEEL": "/flywheel/v0",
      "GPG_KEY": "7169605F62C751356D054A26A821E680E5FA6305",
      "LANG": "C.UTF-8",
      "PATH": "/venv/bin:/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/opt/bin",
      "PWD": "/flywheel/v0",
      "PYTHONPATH": "/venv/lib/python/site-packages",
      "PYTHON_GET_PIP_SHA256": "bc37786ec99618416cc0a0ca32833da447f4d91ab51d2c138dd15b7af21e8e9a",
      "PYTHON_GET_PIP_URL": "https://github.com/pypa/get-pip/raw/def4aec84b261b939137dd1c69eff0aabb4a7bf4/public/get-pip.py",
      "PYTHON_PIP_VERSION": "24.2",
      "PYTHON_SETUPTOOLS_VERSION": "65.5.1",
      "PYTHON_SHA256": "24887b92e2afd4a2ac602419ad4b596372f67ac9b077190f459aba390faf5550",
      "PYTHON_VERSION": "3.12.7",
      "SHLVL": "0",
      "TZ": "UTC",
      "UV_NO_CACHE": "1",
      "VIRTUAL_ENV": "/venv",
      "_": "/bin/printenv"
    },
    "inputs": {
      "api-key": {
        "base": "api-key",
        "read-only": false
      },
      "deid_profile": {
        "base": "file",
        "description": "A Flywheel de-identification profile specifying the de-identification actions to perform.",
        "optional": false,
        "type": {
          "enum": [
            "source code"
          ]
        }
      },
      "subject_csv": {
        "base": "file",
        "description": "A CSV file that contains mapping values to apply for subjects during de-identification.",
        "optional": true,
        "type": {
          "enum": [
            "source code"
          ]
        }
      }
    },
    "label": "De-identified Export",
    "license": "MIT",
    "maintainer": "Flywheel <support@flywheel.io>",
    "name": "deid-export",
    "source": "https://gitlab.com/flywheel-io/scientific-solutions/gears/deid-export/-/blob/main/README.md",
    "url": "https://gitlab.com/flywheel-io/scientific-solutions/gears/deid-export",
    "version": "1.7.0"
  },
  "invocation-schema": {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "properties": {
      "config": {
        "additionalProperties": false,
        "properties": {
          "debug": {
            "default": false,
            "type": "boolean"
          },
          "overwrite_files": {
            "default": "Skip",
            "enum": [
              "Skip",
              "Cleanup",
              "Cleanup_force",
              "Replace"
            ],
            "type": "string"
          },
          "private_key": {
            "type": "string"
          },
          "project_path": {
            "type": "string"
          },
          "public_key": {
            "type": "string"
          },
          "secret_key": {
            "type": "string"
          }
        },
        "required": [
          "debug",
          "overwrite_files",
          "project_path"
        ],
        "type": "object"
      },
      "inputs": {
        "properties": {
          "api-key": {
            "type": "object"
          },
          "deid_profile": {
            "properties": {
              "type": {
                "enum": [
                  "source code"
                ]
              }
            },
            "type": "object"
          },
          "subject_csv": {
            "properties": {
              "type": {
                "enum": [
                  "source code"
                ]
              }
            },
            "type": "object"
          }
        },
        "required": [
          "api-key",
          "deid_profile"
        ],
        "type": "object"
      }
    },
    "required": [
      "config",
      "inputs"
    ],
    "title": "Invocation manifest for De-identified Export",
    "type": "object"
  }
}
