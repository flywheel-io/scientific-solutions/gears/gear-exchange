{
  "exchange": {
    "git-commit": "c1e23846f521aaf7f40701376815f30183806a97",
    "rootfs-hash": "sha256:df6a6ba754c6b0f29f6375fef18e6c6119b2ddada5c8c7be4d4540f6fc7617d0",
    "rootfs-url": "docker://us-docker.pkg.dev/flywheel-exchange/gear-exchange/scitran-fsl-bet:0.2.3"
  },
  "gear": {
    "author": "Analysis Group, FMRIB, Oxford, UK.",
    "config": {
      "apply_mask_thresholding": {
        "default": false,
        "description": "Apply thresholding to segmented brain image and mask.",
        "id": "-t",
        "type": "boolean"
      },
      "binary_brain_mask": {
        "default": false,
        "description": "Generate binary brain mask.",
        "id": "-m",
        "type": "boolean"
      },
      "brain_surf_outline": {
        "default": false,
        "description": "Generate brain surface outline overlaid onto original image.",
        "id": "-o",
        "type": "boolean"
      },
      "fractional_intensity_threshold": {
        "default": 0.5,
        "description": "Fractional intensity threshold (0->1); default=0.5; smaller values give larger brain outline estimates. Changing Fractional intensity threshold from its default value of 0.5 will cause the overall segmented brain to become larger (<0.5) or smaller (>0.5). This threshold must lie between 0 and 1.",
        "id": "-f",
        "maximum": 1,
        "minimum": 0,
        "type": "number"
      },
      "function_option": {
        "default": "",
        "description": "Variations on default bet2 functionality (mutually exclusive). Valid options are: [-R, -S, -B, -Z, -F, -A, -A2].  { -R } robust brain centre estimation (iterates BET several times).  { -S } eye & optic nerve cleanup (can be useful in SIENA).  { -B } bias field & neck cleanup (can be useful in SIENA).  { -Z } improve BET if FOV is very small in Z (by temporarily padding end slices).  { -F } apply to 4D FMRI data (uses -f 0.3 and dilates brain mask slightly).  { -A } run bet2 and then betsurf to get additional skull and scalp surfaces (includes registrations).  { -A2 } as with -A, when also feeding in non-brain-extracted T2 (includes registrations).",
        "enum": [
          "",
          "-R",
          "-S",
          "-B",
          "-Z",
          "-F",
          "-A",
          "-A2"
        ],
        "type": "string"
      },
      "skull_image": {
        "default": false,
        "description": "Generate rough skull image.",
        "id": "-s",
        "type": "boolean"
      },
      "vertical_gradient_intensity_threshold": {
        "default": 0,
        "description": "Vertical gradient in fractional intensity threshold (-1->1); default=0; positive values give larger brain outline at bottom, smaller at top.",
        "id": "-g",
        "maximum": 1,
        "minimum": -1,
        "type": "number"
      },
      "vtk_surface_mesh": {
        "default": false,
        "description": "Generates brain surface as mesh in .vtk format.",
        "id": "-e",
        "type": "boolean"
      }
    },
    "custom": {
      "docker-image": "scitran/fsl-bet:0.2.3",
      "flywheel": {
        "classification": {
          "function": [
            "Image Processing - Segmentation"
          ],
          "modality": [
            "MR"
          ],
          "organ": [
            "Brain"
          ],
          "species": [
            "Human"
          ],
          "therapeutic_area": [
            "Neurology"
          ]
        },
        "suite": "Image Processing"
      },
      "gear-builder": {
        "category": "analysis",
        "image": "scitran/fsl-bet:0.2.3"
      }
    },
    "description": "Brain Extraction Tool (BET2) from FMRIB Software Library (FSL) v5.0. BET (Brain Extraction Tool) deletes non-brain tissue from an image of the whole head. It can also estimate the inner and outer skull surfaces, and outer scalp surface, if you have good quality T1 and T2 input images. LICENSING NOTE: FSL software are owned by Oxford University Innovation and license is required for any commercial applications. For commercial licence please contact fsl@innovation.ox.ac.uk. For academic use, an academic license is required which is available by registering on the FSL website. Any use of the software requires that the user obtain the appropriate license. See https://fsl.fmrib.ox.ac.uk/fsldownloads_registration for more information.",
    "inputs": {
      "nifti": {
        "base": "file",
        "description": "Input NIfTI file for BET algorithm.",
        "type": {
          "enum": [
            "nifti"
          ]
        }
      }
    },
    "label": "FSL: Brain Extraction Tool (BET2)",
    "license": "Apache-2.0",
    "maintainer": "Michael Perry <lmperry@stanford.edu>",
    "name": "fsl-bet",
    "source": "http://fsl.fmrib.ox.ac.uk/fsl/fslwiki/BET",
    "url": "https://github.com/scitran-apps/fsl-bet",
    "version": "0.2.3"
  },
  "invocation-schema": {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "properties": {
      "config": {
        "additionalProperties": false,
        "properties": {
          "apply_mask_thresholding": {
            "default": false,
            "id": "-t",
            "type": "boolean"
          },
          "binary_brain_mask": {
            "default": false,
            "id": "-m",
            "type": "boolean"
          },
          "brain_surf_outline": {
            "default": false,
            "id": "-o",
            "type": "boolean"
          },
          "fractional_intensity_threshold": {
            "default": 0.5,
            "id": "-f",
            "maximum": 1,
            "minimum": 0,
            "type": "number"
          },
          "function_option": {
            "default": "",
            "enum": [
              "",
              "-R",
              "-S",
              "-B",
              "-Z",
              "-F",
              "-A",
              "-A2"
            ],
            "type": "string"
          },
          "skull_image": {
            "default": false,
            "id": "-s",
            "type": "boolean"
          },
          "vertical_gradient_intensity_threshold": {
            "default": 0,
            "id": "-g",
            "maximum": 1,
            "minimum": -1,
            "type": "number"
          },
          "vtk_surface_mesh": {
            "default": false,
            "id": "-e",
            "type": "boolean"
          }
        },
        "required": [
          "fractional_intensity_threshold",
          "brain_surf_outline",
          "binary_brain_mask",
          "skull_image",
          "vertical_gradient_intensity_threshold",
          "apply_mask_thresholding",
          "vtk_surface_mesh",
          "function_option"
        ],
        "type": "object"
      },
      "inputs": {
        "properties": {
          "nifti": {
            "properties": {
              "type": {
                "enum": [
                  "nifti"
                ]
              }
            },
            "type": "object"
          }
        },
        "required": [
          "nifti"
        ],
        "type": "object"
      }
    },
    "required": [
      "config",
      "inputs"
    ],
    "title": "Invocation manifest for FSL: Brain Extraction Tool (BET2)",
    "type": "object"
  }
}
