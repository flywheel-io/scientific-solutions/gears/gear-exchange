{
  "author": "van Griethuysen, J. J. M. et al.",
  "cite": "van Griethuysen, J. J. M., Fedorov, A., Parmar, C., Hosny, A., Aucoin, N., Narayan, V., Beets-Tan, R. G. H., Fillon-Robin, J. C., Pieper, S., Aerts, H. J. W. L. (2017). Computational Radiomics System to Decode the Radiographic Phenotype. Cancer Research, 77(21), e104–e107.",
  "command": "python run.py",
  "config": {
    "convert_numpy_to_single_channel": {
      "default": false,
      "description": "Converts 3D numpy arrays that have 3 images along the third dimension--each representing a color channel--to single-channel arrays. Only the first image will be used to calculate radiomic features.",
      "type": "boolean"
    },
    "debug": {
      "default": false,
      "description": "Log debug messages",
      "type": "boolean"
    },
    "voxel_based": {
      "default": false,
      "description": "Not implemented yet (leave as false). Perform voxel-based calculations. The output is a SimpleITK image of the parameter map instead of a float value for each feature. Voxel-based calculations take a significant longer time; features have to be calculated for each voxel.",
      "type": "boolean"
    }
  },
  "custom": {
    "flywheel": {
      "classification": {
        "function": [
          "Image Processing - Other"
        ],
        "modality": [
          "CT",
          "MR"
        ],
        "organ": [
          "Multiple"
        ],
        "species": [
          "Phantom",
          "Human",
          "Animal"
        ],
        "therapeutic_area": [
          "Oncology"
        ]
      },
      "suite": "Image Processing"
    },
    "gear-builder": {
      "category": "analysis",
      "image": "flywheel/pyradiomics:2.1.0_3.0.2"
    }
  },
  "description": "Extraction of Radiomics features from medical imaging data using the PyRadiomics module. Supports feature extraction for 2D and 3D images; a mask is required to calculate single values per feature for a region of interest (“segment-based”). If no mask is passed, then (almost, see *mask* input) the entire input image will be used as to generate features.  Extracted features are output in a CSV file. Voxel-based radiomics are not supported at this time.",
  "environment": {
    "FLYWHEEL": "/flywheel/v0",
    "GPG_KEY": "E3FF2839C048B25C084DEBE9B26995E310250568",
    "LANG": "C.UTF-8",
    "PATH": "/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
    "PWD": "/flywheel/v0",
    "PYTHON_GET_PIP_SHA256": "ee09098395e42eb1f82ef4acb231a767a6ae85504a9cf9983223df0a7cbd35d7",
    "PYTHON_GET_PIP_URL": "https://github.com/pypa/get-pip/raw/e03e1607ad60522cf34a92e834138eb89f57667c/public/get-pip.py",
    "PYTHON_PIP_VERSION": "23.0.1",
    "PYTHON_SETUPTOOLS_VERSION": "58.1.0",
    "PYTHON_VERSION": "3.9.20"
  },
  "inputs": {
    "image": {
      "base": "file",
      "description": "A 2D or 3D image that is supported by SimpleITK."
    },
    "mask": {
      "base": "file",
      "description": "A mask to be used for extracting pyRadiomics features and is supported by SimpleITK. If no mask is passed,\nthen a mask will be created that uses the entire image except the top-left pixel that is located at (0,0). \n  PyRadiomics requires masks to have at least one pixel that has a different intensity value than the rest.",
      "optional": true
    },
    "params_file": {
      "base": "file",
      "description": "A configuration file for configuring the pyRadiomics feature extractor. If no file is passed, then the default configuration file will be used.",
      "optional": true
    }
  },
  "label": "PyRadiomics",
  "license": "BSD-3-Clause",
  "maintainer": "Flywheel <support@flywheel.io>",
  "name": "pyradiomics",
  "source": "https://pyradiomics.readthedocs.io/en/latest/index.html",
  "url": "https://gitlab.com/flywheel-io/scientific-solutions/gears/pyradiomics",
  "version": "2.1.0_3.0.2"
}
