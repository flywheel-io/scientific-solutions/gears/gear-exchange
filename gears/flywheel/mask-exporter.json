{
  "author": "Flywheel",
  "cite": "",
  "command": "python /flywheel/v0/run.py",
  "config": {
    "additional-arguments": {
      "description": "Expert Option: Additional arguments to be passed through to the converter (Dicom input only).",
      "optional": true,
      "type": "string"
    },
    "all-file-versions": {
      "default": false,
      "description": "Whether to exporter ROIs from all file versions, not just the latest file version. Default False.",
      "type": "boolean"
    },
    "annotations-scope": {
      "default": "both",
      "description": "Scope of ROIs to save: task, non-task, or both. Default both.",
      "enum": [
        "task",
        "non-task",
        "both"
      ],
      "type": "string"
    },
    "completed-task-only": {
      "default": true,
      "description": "Whether to export only completed read task ROIs (ignores incomplete read tasks). Default True.",
      "type": "boolean"
    },
    "debug": {
      "default": false,
      "description": "Log debug messages",
      "type": "boolean"
    },
    "read-timeout": {
      "default": 60,
      "description": "The maximum time in seconds to wait for a response from the server. Default 60, minimum 60.",
      "type": "integer"
    },
    "save-binary-masks": {
      "default": true,
      "description": " Saves individually labeled ROIs as binary masks. Otherwise use bitmasked values (sums of powers of two). Default is True.",
      "type": "boolean"
    },
    "save-combined-output": {
      "default": "individual",
      "description": "Whether to save each task's ROIs as individual files by label, combined, or both. Default individual.",
      "enum": [
        "individual",
        "combined",
        "both"
      ],
      "type": "string"
    },
    "save-nrrd": {
      "default": false,
      "description": "Saves the ROI's as output type NRRD instead of NIfTI.  Default False.",
      "type": "boolean"
    }
  },
  "custom": {
    "flywheel": {
      "classification": {
        "function": [
          "Export"
        ],
        "modality": [
          "Any"
        ],
        "organ": [
          "Any"
        ],
        "species": [
          "Phantom",
          "Human",
          "Animal",
          "Other"
        ],
        "therapeutic_area": [
          "Any"
        ]
      },
      "suite": "Export"
    },
    "gear-builder": {
      "category": "analysis",
      "image": "flywheel/mask-exporter:1.2.3"
    }
  },
  "description": "This gear converts ROIs created in Flywheel's OHIF viewer to NIfTI files.",
  "environment": {
    "BUILD_TIME": "2024-09-18T22:35:43Z",
    "COMMIT_REF": "main",
    "COMMIT_SHA": "8a826b41",
    "CXX": "/usr/bin/gcc",
    "DCMCOMMIT": "fe2f26005109f396a4f828aa438241f73fc25fe8",
    "DEBIAN_FRONTEND": "noninteractive",
    "EDITOR": "micro",
    "EGET_BIN": "/opt/bin",
    "FLYWHEEL": "/flywheel/v0",
    "GPG_KEY": "7169605F62C751356D054A26A821E680E5FA6305",
    "HOME": "/root",
    "LANG": "C.UTF-8",
    "OJ_VERSION": "2.4.0",
    "OPENJPEGDIR": "/flywheel/v0/openjpeg",
    "PATH": "/venv/bin:/opt/bin:/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
    "PROMETHEUS_MULTIPROC_DIR": "/var/metrics",
    "PWD": "/flywheel/v0",
    "PYSITE": "/usr/local/lib/python3.11/site-packages",
    "PYTHONPATH": "/venv/lib/python/site-packages",
    "PYTHON_GET_PIP_SHA256": "bc37786ec99618416cc0a0ca32833da447f4d91ab51d2c138dd15b7af21e8e9a",
    "PYTHON_GET_PIP_URL": "https://github.com/pypa/get-pip/raw/def4aec84b261b939137dd1c69eff0aabb4a7bf4/public/get-pip.py",
    "PYTHON_PIP_VERSION": "24.2",
    "PYTHON_SETUPTOOLS_VERSION": "65.5.1",
    "PYTHON_VERSION": "3.12.6",
    "SETUPTOOLS_USE_DISTUTILS": "stdlib",
    "SHLVL": "1",
    "TERM": "xterm",
    "TZ": "UTC",
    "UV_NO_CACHE": "1",
    "VIRTUAL_ENV": "/venv",
    "XDG_RUNTIME_DIR": "/tmp/runtime-sliceruser",
    "_": "/bin/printenv"
  },
  "inputs": {
    "api-key": {
      "base": "api-key",
      "read-only": false
    },
    "input-file": {
      "base": "file",
      "description": "NIfTI or DICOM file with ROI drawn in OHIF Viewer (Required).",
      "optional": false,
      "type": {
        "enum": [
          "nifti",
          "dicom"
        ]
      }
    }
  },
  "label": "Mask Exporter",
  "license": "MIT",
  "maintainer": "Flywheel <support@flywheel.io>",
  "name": "mask-exporter",
  "source": "https://gitlab.com/flywheel-io/scientific-solutions/gears/mask-exporter",
  "url": "https://gitlab.com/flywheel-io/scientific-solutions/gears/mask-exporter",
  "version": "1.2.3"
}
