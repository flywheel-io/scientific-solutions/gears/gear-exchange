import json
import sys
from functools import partial
from pathlib import Path
from typing import List
import git
from datetime import datetime


def remove_nested_keys(dictionary: dict, nested_keys: List[str]) -> dict:
    current = dictionary
    for key in nested_keys[:-1]:
        if key in current:
            current = current[key]
        else:
            return dictionary

    current.pop(nested_keys[-1], None)

    return dictionary


def remove_extra_keys(dictionary: dict) -> dict:
    for key in ["config", "inputs", "flywheel"]:
        dictionary.pop(key, None)

    updated_dict = remove_nested_keys(dictionary, ["custom", "gear-builder", "image"])

    return updated_dict


def remove_nulls(dictionary: dict) -> dict:
    if isinstance(dictionary, dict):
        return {
            k: remove_nulls(v) for k, v in dictionary.items() if v and remove_nulls(v)
        }
    if isinstance(dictionary, list):
        return [remove_nulls(v) for v in dictionary if v and remove_nulls(v)]
    return dictionary


def test_remove_nulls():
    assert remove_nulls({"a": ["b", None]}) == {"a": ["b"]}
    assert remove_nulls({"a": {"b": None}}) == {}
    assert remove_nulls({"a": None}) == {}


def add_path_key(dictionary: dict, file_path: Path) -> dict:
    dictionary["exchange-path"] = str(file_path)
    return dictionary


def add_commit_key(dictionary: dict, commit_hash: str) -> dict:
    dictionary["git-commit"] = commit_hash
    return dictionary


def add_commit_created_date(dictionary: dict, repo: git.Repo) -> dict:
    commit = repo.commit(dictionary["git-commit"])
    dictionary["git-commit-created"] = commit.committed_date
    return dictionary


def group_and_sort(gears: List[dict]) -> List[dict]:
    gears.sort(key=lambda d: d["name"])
    groups = {}
    for gear in gears:
        groups.setdefault(gear.get("name"), []).append(gear)

    sorted_gears = []
    for group in groups.values():
        sorted_gears.append(sorted(group, key=lambda v: v["version"], reverse=True))
    return sorted_gears


def tag_latest_commit(repo: git.Repo, group: List[dict]):
    key = "git-commit"
    hash_dates = {v[key]: repo.commit(v[key]).committed_date for v in group}
    latest_hash = max(hash_dates, key=hash_dates.get)
    new_group = []
    for gear in group:
        if latest_hash == gear[key]:
            gear["latest"] = True
        else:
            gear["latest"] = False
        del gear[key]
        new_group.append(gear)
    return new_group


def filter_duplicate_versions(group: List[dict]):
    unique_gears = {}
    for gear in group:
        key = (gear["name"], gear["version"])
        if (
            key not in unique_gears
            or gear["git-commit-created"] > unique_gears[key]["git-commit-created"]
        ):
            unique_gears[key] = gear
    return list(unique_gears.values())


def format_commit_created_date(group: List[dict]) -> str:
    for gear in group:
        gear["git-commit-created"] = datetime.fromtimestamp(
            gear["git-commit-created"]
        ).strftime("%Y-%m-%d %H:%M:%S")
    return group


def main(manifest_dir: str, save_path: str):
    gears = []
    repo = git.Repo(".")

    for manifest_path in Path(manifest_dir).rglob("*.json"):
        with open(manifest_path, "r") as f:
            manifest = json.load(f)

        gear = manifest["gear"]

        funcs = [
            remove_extra_keys,
            remove_nulls,
            partial(add_path_key, file_path=manifest_path),
            partial(add_commit_key, commit_hash=manifest["exchange"]["git-commit"]),
            partial(add_commit_created_date, repo=repo),
        ]

        for func in funcs:
            gear = func(gear)

        gears.append(gear)

    grouped_gears = group_and_sort(gears)

    for i, group in enumerate(grouped_gears):
        group = filter_duplicate_versions(group)
        group = tag_latest_commit(repo, group)
        group = format_commit_created_date(group)
        grouped_gears[i] = group

    with open(save_path, "w") as f:
        json.dump(grouped_gears, f, indent=2)


if __name__ == "__main__":
    main(sys.argv[1], sys.argv[2])
