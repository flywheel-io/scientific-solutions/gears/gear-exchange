#!/usr/bin/env bash

# Set strict error handling
set -eEuo pipefail

# Directory setup
DIR=$(cd "${0%/*}/.." && pwd)
PWD=$(pwd)
PS4='+ ${BASH_SOURCE//$PWD\//}:${LINENO} ${FUNCNAME[0]:+${FUNCNAME[0]}(): }'
test -z "${DEBUG:-}" || set -x

# Constants
GEARS_DIR="gears"
MANIFESTS_DIR="manifests"
SENTINEL_FILENAME=".sentinel"
EXCHANGE_JSON="exchange.json"
GEAR_SCHEMA_URL="https://gitlab.com/flywheel-io/public/gears/-/raw/master/spec/manifest.schema.json"
EXCHANGE_ARTIFACT_REGISTRY_URL="us-docker.pkg.dev/flywheel-exchange/gear-exchange"
FLYWHEEL_DOCKER_REGISTRY_URL="docker.io/flywheel"
GIT_REMOTE=${GIT_REMOTE:-"origin"}
GIT_BRANCH="$(git rev-parse --abbrev-ref HEAD)"
GIT_COMMIT_CURRENT=$(git rev-parse HEAD)
GIT_COMMIT_SENTINEL=$(cat $SENTINEL_FILENAME 2> /dev/null || true)

BUILD_ARTIFACTS=""
EXIT_STATUS=0

# Functions
git_config_cleanup() {
    git config --local --remove-section user 2>/dev/null || true
}
trap git_config_cleanup EXIT

gear_version_already_exists() {
    if [ "$#" -ne 3 ]; then
        >&2 echo "Invalid number of positional arguments in gear_version_already_exists()"
        exit 1
    fi

    local GEAR_ORG="$1"
    local GEAR_NAME="$2"
    local GEAR_VERSION="$3"

    if [ -d "$MANIFESTS_DIR/$GEAR_ORG" ]; then
        for FILE in "$MANIFESTS_DIR/$GEAR_ORG/"*.json ; do
            local V_GEAR_NAME="$(jq -r '.gear.name' $FILE)"
            local V_GEAR_VERSION="$(jq -r '.gear.version' $FILE)"

            if [[ "$GEAR_NAME" == "$V_GEAR_NAME" && "$GEAR_VERSION" == "$V_GEAR_VERSION" ]] ; then
                >&2 echo "Strongly versioned gear '$GEAR_NAME' of version '$GEAR_VERSION' found in file '$FILE'"
                return 0
            fi
        done
    fi
    return 1
}

validate_manifest() {
    if [[ "$2" != *.json ]]; then
        >&2 echo "Manifest files must have a .json file name extension."
        exit 1
    fi
    

    if [ "$1" == "gear" ]; then
        GEAR_NAME="$(jq -r '.name' $2)"
        GEAR_VERSION="$(jq -r '.version' $2)"
        GEAR_DIR="${2%/*}" 
        GEAR_ORG="${GEAR_DIR##*/}"  
        if gear_version_already_exists "$GEAR_ORG" "$GEAR_NAME" "$GEAR_VERSION"; then
            if [ "$OVERRIDE_PUBLISH" == "true" ]; then
                >&2 echo "Error: Candidate gear already strongly versioned BUT override is being enforced. Ignoring..."
            else
                >&2 echo "Error: Candidate gear already strongly versioned. Submit the gear to the exchange with a unique version." && exit 1 
            fi
        fi

        if [ ! -v GEAR_SCHEMA_PATH ]; then
            >&2 echo "Installing gear schema"
            GEAR_SCHEMA_PATH=$(mktemp)
            curl -s $GEAR_SCHEMA_URL > $GEAR_SCHEMA_PATH
        fi
        check-jsonschema --schemafile $GEAR_SCHEMA_PATH "$2"

        DOCKER_IMAGE="$(jq -r '.custom."gear-builder".image' $2)"
        if [ "$DOCKER_IMAGE" == 'null' ]; then
            DOCKER_IMAGE="$(jq -r '.custom."docker-image"' $2)"
        fi

        # Split the image name into root (e.g., flywheel/validated-file-metadata-importer)
        # and tag/version (e.g, 0.0.1). Throw an error if the image name is not tagged.
        IFS=':' read -ra _DOCKER_IMAGE <<< "${DOCKER_IMAGE}"
        IMAGE_ROOT=${_DOCKER_IMAGE[0]}
        IMAGE_TAG=${_DOCKER_IMAGE[1]:-}
        if [[ -z $IMAGE_TAG ]]; then
            echo "A tagged image is required!" && exit 1
        fi

        # Verify the image exists in the docker registry. Get all image tags if so.
        url="https://registry.hub.docker.com/v2/repositories/${IMAGE_ROOT}/tags/"
        ALL_TAGS=""
        while [[ -n "${url}" ]]; do
            RESPONSE=$(curl -s -S -H "Authorization: Bearer $DOCKER_TOKEN" "${url}")
            if echo "${RESPONSE}" | grep -qi "object not found"; then
                echo "Image: \"${DOCKER_IMAGE}\" does not exist" && exit 1
            fi

            TAGS=$(echo "${RESPONSE}" | jq -r '."results"[]["name"]')
            ALL_TAGS="${ALL_TAGS}${TAGS}\n"
            url=$(echo "${RESPONSE}" | jq -r '.next // empty')
        done

        # Verify that the specified image tag exists in the list of all tags
        IMAGE_INFO=$(echo -e "${ALL_TAGS}" | sort | uniq)
        if [[ -n "${IMAGE_TAG}" && $(echo "${IMAGE_INFO}" | grep -Fx "${IMAGE_TAG}") == "" ]]; then
            echo "Specified image tag: \"${IMAGE_TAG}\" does not exist for image \"${IMAGE_ROOT}\"" && exit 1
        fi
    else
        >&2 echo "Manifest validation for type \"$1\" not implemented"
        return 1
    fi
}

validate_manifests() {

    for MANIFEST_PATH in $1; do
        MANIFEST_TYPE="${MANIFEST_PATH%%s/*}"
        MANIFEST_NAME="${MANIFEST_PATH#*/}"
        MANIFEST_NAME="${MANIFEST_NAME%.json}"
        >&2 echo "Validating $MANIFEST_TYPE $MANIFEST_NAME"
        validate_manifest "$MANIFEST_TYPE" "$MANIFEST_PATH"
    done
}

derive_invocation_schema() {

    if [ "$1" == "gear" ]; then
        echo $(python bin/generate_invocation_schema.py "$2")            
    elif [ "$1" == "boutique" ]; then
        echo "{\"WARNING\": \"Invocation schema validation for boutiques not yet implemented\"}"
    else
        >&2 echo "Invocation schema generation for type \"$1\" not implemented"
        return 1
    fi
}

cleanup() {
    >&2 echo "Restoring git commit history"
    git reset --hard $GIT_COMMIT_CURRENT
    >&2 echo "Attempting to remove build artifacts"
    gsutil rm $BUILD_ARTIFACTS
    gcloud container images delete --quiet $exchange_image
    >&2 echo "Build artifacts removed successfully"
}

process_manifests() {
    >&2 echo "Processing manifests..."
    >&2 echo "Checking out branch $CI_COMMIT_REF_NAME"
    git checkout $CI_COMMIT_REF_NAME
    git pull origin $CI_COMMIT_REF_NAME
    

    for MANIFEST_PATH in $1; do
        MANIFEST_TYPE="${MANIFEST_PATH%%s/*}"
        MANIFEST_NAME="${MANIFEST_PATH#*/}"
        MANIFEST_NAME="${MANIFEST_NAME%.json}"
        MANIFEST_HIER="/$MANIFEST_NAME"
        MANIFEST_HIER="${MANIFEST_HIER%/*}"
        MANIFEST_SLUG="${MANIFEST_NAME//\//-}"
        >&2 echo "Processing manifest $MANIFEST_TYPE $MANIFEST_NAME"

        if ! validate_manifest "$MANIFEST_TYPE" "$MANIFEST_PATH"; then
            >&2 echo "Schema validation failed for $MANIFEST_TYPE $MANIFEST_NAME"
            EXIT_STATUS=1
            cleanup
        else
            >&2 echo "Schema successfully validated for $MANIFEST_TYPE $MANIFEST_NAME"

            local TEMPDIR=$(mktemp -d)
            local TEMPFILE=$TEMPDIR/tempfile

            if [ "$MANIFEST_TYPE" == "gear" ]; then
                DOCKER_IMAGE="$(jq -r '.custom."gear-builder".image' $MANIFEST_PATH)"
                if [ "$DOCKER_IMAGE" == 'null' ]; then
                    DOCKER_IMAGE="$(jq -r '.custom."docker-image"' $MANIFEST_PATH)"
                fi
                MANIFEST_VERSION="$(jq -r '.version' $MANIFEST_PATH)"
                PRIVATE_REPO_BOOL="$(jq -r '.custom.flywheel.private' $MANIFEST_PATH)"
                GEAR_NAME="$(jq -r '.name' $MANIFEST_PATH)"
            else
                DOCKER_IMAGE="$(jq -r '."container-image"."image"' $MANIFEST_PATH)"
                MANIFEST_VERSION=""
            fi
            >&2 echo "Docker image: ${DOCKER_IMAGE}"
            >&2 echo "Manifest version: ${MANIFEST_VERSION}"

            >&2 echo "Skipping beta exchange"

            # Do not copy the image to the public docker registry if it is a private repo
            if [ "$PRIVATE_REPO_BOOL" == "true" ]; then
                # Split the image name into root (e.g., flywheel/validated-file-metadata-importer)
                # and tag/version (e.g, 0.0.1).
                IFS=':' read -ra _DOCKER_IMAGE <<< "${DOCKER_IMAGE}"
                IMAGE_ROOT=${_DOCKER_IMAGE[0]}
                IMAGE_TAG=${_DOCKER_IMAGE[1]:-}

                local IMAGE_NAME="$FLYWHEEL_DOCKER_REGISTRY_URL/$DOCKER_IMAGE" # can't use MANIFEST_SLUG b/c it has a dash after the org
                >&2 echo "IMAGE_NAME: $IMAGE_NAME"
                >&2 echo "The repository for $GEAR_NAME is private, not copying its docker image to the publicly accessible docker registry."

                # Get the digest of the image form the private docker registry
                local DIGEST_VAL=$(curl -s -H "Authorization: Bearer $DOCKER_TOKEN" "https://registry.hub.docker.com/v2/repositories/$IMAGE_ROOT/tags/$IMAGE_TAG" | jq -r '.images[0].digest' | grep -oP '[a-f0-9]{64}')
            else
                local IMAGE_NAME="$EXCHANGE_ARTIFACT_REGISTRY_URL/$MANIFEST_SLUG:$MANIFEST_VERSION"
                >&2 echo "IMAGE_NAME: $IMAGE_NAME"
                >&2 echo "Copying docker image $DOCKER_IMAGE to $IMAGE_NAME"
                skopeo copy --multi-arch=all "docker://$DOCKER_IMAGE" "docker://$IMAGE_NAME" || { echo "Failed to copy docker image $DOCKER_IMAGE to $IMAGE_NAME" && cleanup && exit 1; }

                # Get the digest of the image form the public docker registry
                local DIGEST_VAL=$(gcloud container images describe $IMAGE_NAME --format='value(image_summary.digest)' | grep -oP '[a-f0-9]{64}')
            fi

            >&2 echo "DIGEST_VAL: $DIGEST_VAL"

            local SHASUM="sha256:${DIGEST_VAL}"
            echo "SHASUM: $SHASUM"
            local V_MANIFEST_NAME="${MANIFEST_SLUG}-sha256-${DIGEST_VAL}"
            >&2 echo "V_MANIFEST_NAME: $V_MANIFEST_NAME"

            >&2 echo "GIT_COMMIT_CURRENT: $GIT_COMMIT_CURRENT"
            local V_MANIFEST_PATH="${MANIFESTS_DIR}/${MANIFEST_HIER}/${V_MANIFEST_NAME}.json"
            mkdir -p "$MANIFESTS_DIR/$MANIFEST_HIER"
            jq "{\"$MANIFEST_TYPE\": .}" ${MANIFEST_PATH} > ${V_MANIFEST_PATH}
            jq ".exchange.\"git-commit\" = \"$GIT_COMMIT_CURRENT\"" ${V_MANIFEST_PATH} > $TEMPFILE && mv $TEMPFILE ${V_MANIFEST_PATH}
            jq ".exchange.\"rootfs-hash\" = \"$SHASUM\" | .exchange.\"rootfs-url\" = \"docker://$IMAGE_NAME\"" $V_MANIFEST_PATH > $TEMPFILE && mv $TEMPFILE ${V_MANIFEST_PATH}

            local INVOCATION_SCHEMA=$(derive_invocation_schema "$MANIFEST_TYPE" "$MANIFEST_PATH")
            >&2 echo "Invocation schema generated for $MANIFEST_TYPE $MANIFEST_NAME"
            jq --argjson content "$(cat $INVOCATION_SCHEMA)" '.["invocation-schema"] = $content' $V_MANIFEST_PATH > $TEMPFILE && mv $TEMPFILE ${V_MANIFEST_PATH}
            cat ${V_MANIFEST_PATH}

            git add $V_MANIFEST_PATH
            echo $GIT_COMMIT_CURRENT > $SENTINEL_FILENAME
            cat $SENTINEL_FILENAME
            git add $SENTINEL_FILENAME
            echo "Process $MANIFEST_TYPE $MANIFEST_NAME $MANIFEST_VERSION"
            git commit -m "Process $MANIFEST_TYPE $MANIFEST_NAME $MANIFEST_VERSION"

            rm -rf $TEMPDIR
        fi
    done

    if git push --force-with-lease $GIT_REMOTE HEAD:$CI_COMMIT_REF_NAME; then
        >&2 echo "Git push successful"
    else
        >&2 echo "Git push failed"
        EXIT_STATUS=1
        cleanup
        exit 1
    fi
}

publish_global_manifest() {
    >&2 echo "Publish global manifest"
    git fetch --unshallow || git fetch --all
    python bin/build_exchange_json.py $MANIFESTS_DIR .$EXCHANGE_JSON
    git fetch origin gh-pages-json
    git checkout gh-pages-json
    mv -f .$EXCHANGE_JSON $EXCHANGE_JSON
    git add $EXCHANGE_JSON
    git commit --amend --reset-author -m "Add exchange.json"
    git push -f $GIT_REMOTE gh-pages-json
    git checkout $GIT_BRANCH
}

get_manifests_list() {
    >&2 echo "On branch $CI_COMMIT_REF_NAME"

    # Check if override is not set and git commit not set
    if [ -z "$GIT_COMMIT_SENTINEL" ] && [ "$OVERRIDE" != "true" ]; then
        >&2 echo "Using all manifests"
        MANIFESTS=$(find "$GEARS_DIR" -iname "*.json")
        >&2 echo "$MANIFESTS"
    else
        >&2 echo "Using updated manifests"
        MANIFESTS=$(git diff --name-only --diff-filter=d "$GIT_COMMIT_SENTINEL" | grep "^$GEARS_DIR/..*$" || true)
    fi

    if [ -z "$MANIFESTS" ]; then
        >&2 echo "No manifests to process or validate"
    else  
        >&2 echo "Found updated manifests to process:"
        >&2 echo "$MANIFESTS"
    fi

    export MANIFESTS
}

# Main script execution
if ! $(git status &> /dev/null); then
    >&2 echo "This script must be run from within a git repo."
    exit 1
fi
if ! $(git diff-index --quiet HEAD --); then
    >&2 echo "This script can only be run in a clean git repo."
    exit 1
fi

if ! pip show gitpython &> /dev/null; then
    echo "Installing GitPython..."
    pip install gitpython --cache-dir=.cache/pip
else
    echo "GitPython is already installed."
fi

if ! pip show check-jsonschema &> /dev/null; then
    echo "check-jsonschema is not installed. Installing now..."
    pip install check-jsonschema --cache-dir=.cache/pip
else
    echo "check-jsonschema is already installed."
fi

/qa-ci/scripts/run.sh job git_login
source /qa-ci/scripts/jobs.sh
gcloud_install

if [[ -z "$EXCHANGE_SERVICE_ACCOUNT" ]]; then
    echo "Error: EXCHANGE_SERVICE_ACCOUNT variable is not set."
    exit 1
fi

# Authenticate with gcloud
gcloud auth activate-service-account --key-file $EXCHANGE_SERVICE_ACCOUNT
gcloud auth configure-docker us-docker.pkg.dev --quiet
gcloud config set project flywheel-exchange

# Authenticate with docker
if [ ! -z "$DOCKER_CI_USER" -a ! -z "$DOCKER_CI_PASS" ]; then
    echo "$DOCKER_CI_PASS" | docker login -u "$DOCKER_CI_USER" --password-stdin
fi

# Get a docker token to access private docker repos with curl
DOCKER_TOKEN=$(curl -s -H "Content-Type: application/json" -X POST -d "{\"username\": \"$DOCKER_CI_USER\", \"password\": \"$DOCKER_CI_PASS\"}" https://hub.docker.com/v2/users/login/ | jq -r .token)

get_manifests_list
>&2 echo "Exported manifests variable: $MANIFESTS"

if [ "${CI_COMMIT_REF_NAME}" == "master" ]; then
    >&2 echo "On master branch..."
    set -eu
    if [ -n "$MANIFESTS" ]; then
        process_manifests "$MANIFESTS"
    else
        >&2 echo "No manifests to process."
    fi
    publish_global_manifest
    >&2 echo "Successfully processed manifest..."
else
    >&2 echo "Validating manifest and docker image..."
    set -eu
    validate_manifests "$MANIFESTS"
    >&2 echo "Manifest has been validated..."
fi

exit $EXIT_STATUS
