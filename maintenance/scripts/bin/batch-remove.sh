#!/bin/bash
set -Eeuo pipefail
# batch-remove.sh - Batch remove gears using a Python script fromt a list of gear names in a text file.

# Usage Description
USAGE="Usage: $0
To run this script, navigate to the directory where it's located, and execute:
  ./batch-remove.sh

- Ensure that '$GEAR_NAMES_FILE' exists and contains the names of gears, one per line.
- Ensure that '$PYTHON_SCRIPT_PATH' exists and is executable.
- This script reads each gear name from '$GEAR_NAMES_FILE' and calls the Python script at '$PYTHON_SCRIPT_PATH' to remove each gear.

"
# Path to your Python script
PYTHON_SCRIPT_PATH="../remove-gear.py"

# Path to the text file containing the gear names (one per line)
GEAR_NAMES_FILE="../assets/gear_names.txt"

validate_files() {
    # Check if Python script exists and is executable
    if [ ! -x "$PYTHON_SCRIPT_PATH" ]; then
        echo "Error: Python script at $PYTHON_SCRIPT_PATH is not executable."
        echo "$USAGE"
        exit 1
    fi

    # Check if the text file exists and is not empty
    if [ ! -f "$GEAR_NAMES_FILE" ] || [ ! -s "$GEAR_NAMES_FILE" ]; then
        echo "Error: Gear names file at $GEAR_NAMES_FILE is missing or empty."
        echo "$USAGE"
        exit 1
    fi
}

run_gear_removal() {
    # Read each line from the text file
    while IFS= read -r gear_name
    do
        echo "Removing gear: $gear_name"
        python "$PYTHON_SCRIPT_PATH" "$gear_name" || echo "Failed to remove gear: $gear_name"
    done < "$GEAR_NAMES_FILE"
}

main() {
    echo "$*" | grep -Eqw "help|--help|-h" && { echo "$USAGE"; exit 0; }
    # Validate necessary files
    validate_files
    run_gear_removal
}

main "$@"