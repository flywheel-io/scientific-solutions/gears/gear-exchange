#!/usr/bin/env python3
"""Export a CSV file with the inventory of all gears in the exchange."""

import json
import logging
import typing as t
from datetime import datetime
from pathlib import Path

import pandas as pd
from box import Box

logging.basicConfig(level=logging.INFO)
log = logging.getLogger()


GEAR_ROOT = Path(__file__).parents[2].absolute() / "gears"


def is_api_key_enabled(manifest: dict) -> bool:
    inputs = manifest.get("inputs", {})
    for k, v in inputs.items():
        if v.get("base", "") == "api-key":
            return True
    return False


def get_api_key_perm(manifest: dict) -> str:
    inputs = manifest.get("inputs", {})
    for k, v in inputs.items():
        if v.get("base", "") == "api-key":
            if "read-only" in v.keys():
                return "ro"
            else:
                return "rw"
    return "N/A"


def get_file_types(manifest: dict) -> t.Dict[str, t.Union[str, list[str]]]:
    inputs = manifest.get("inputs", {})
    file_types = {}
    for k, v in inputs.items():
        if v.get("base", "") == "file":
            # if "type" is not specify, save as N/A
            if "type" not in v.keys():
                file_types[k] = "N/A"
                continue
            # if "type" is specify, we expect "enum" with a list of file types...
            f_type = v["type"].get("enum", [])
            # ... but if it's not, we save as N/A
            if len(f_type) == 0:
                file_types[k] = "N/A"
                continue
            # the file type is correctly specified as a list. Cast as list (currently
            # it's of type "BoxList")
            file_types[k] = list(f_type)
    return file_types


COL_JSON_MAP = {
    "name": lambda manifest: manifest.get("name"),
    "label": lambda manifest: manifest.get("label"),
    "version": lambda manifest: manifest.get("version"),
    "category": lambda manifest: manifest.get("custom.gear-builder.category"),
    "image": lambda manifest: manifest.get("custom.gear-builder.image"),
    "api_key": lambda manifest: is_api_key_enabled(manifest),
    "key_perm": lambda manifest: get_api_key_perm(manifest),
    "file_types": lambda manifest: get_file_types(manifest),
    "license": lambda manifest: manifest.get("license"),
    "author": lambda manifest: manifest.get("author"),
    "maintainer": lambda manifest: manifest.get("maintainer"),
    "source": lambda manifest: manifest.get("source"),
    "url": lambda manifest: manifest.get("url"),
    "description": lambda manifest: manifest.get("description"),
    "suite": lambda manifest: manifest.get("custom.flywheel.suite"),
    "classification.function": lambda manifest: manifest.get(
        "custom.flywheel.classification.function"
    ),
    "classification.modality": lambda manifest: manifest.get(
        "custom.flywheel.classification.modality"
    ),
    "classification.organ": lambda manifest: manifest.get(
        "custom.flywheel.classification.organ"
    ),
    "classification.species": lambda manifest: manifest.get(
        "custom.flywheel.classification.species"
    ),
    "classification.therapeutic_area": lambda manifest: manifest.get(
        "custom.flywheel.classification.therapeutic_area"
    ),
}


def extract_data_from_manifest_json(file_path: Path) -> list:
    with open(file_path, "r") as f:
        manifest = Box(json.load(f), box_dots=True)
        return [fn(manifest) for fn in COL_JSON_MAP.values()]


def is_flywheel_maintained(url: str) -> t.Union[bool, None]:
    if url is None:
        return None
    if "github.com/flywheel-apps" in url:
        return True
    if "gitlab.com/flywheel-io" in url:
        return True
    if "github.com/scitran-apps" in url:
        return True
    return False


def needs_migration(url: str) -> bool:
    if "gitlab.com/flywheel-io/scientific-solutions" not in url:
        return True
    return False


def print_summary_report(df: pd.DataFrame):
    log.info(f"Total gears: {len(df)}")
    log.info(f"Flywheel Historic: {df['fw_historic'].sum()}")
    log.info(
        f"Flywheel Maintained: {df[df['maintainer'] == 'Flywheel <support@flywheel.io>'].shape[0]}"
    )
    log.info(f"Needs migration: {df['needs_migration'].sum()}")
    log.info(f"Category stats: \n{df['category'].value_counts()}")
    log.info(f"Modality stats:\n{df['classification.modality'].value_counts()}")
    log.info(f"Suite stats:\n{df['suite'].value_counts()}")


def main():
    output_dir = Path(__file__).parent / "reports"
    output_dir.mkdir(exist_ok=True)
    output_csv = (
        output_dir / f"gear-exchange-inventory-{datetime.now().date().isoformat()}.csv"
    )

    df = pd.DataFrame(columns=list(COL_JSON_MAP.keys()) + ["modified"])
    for folder in GEAR_ROOT.iterdir():
        folder_dir = GEAR_ROOT / folder
        for file_path in folder_dir.rglob("*.json"):
            # turns out that some gears are defined in multiple subfolders
            # so grabbing the modified date to only keep the most recent
            timestamp = file_path.stat().st_mtime
            modified_date = datetime.fromtimestamp(timestamp)
            manifest_data = extract_data_from_manifest_json(file_path)
            manifest_data.append(modified_date.isoformat())
            df.loc[len(df)] = manifest_data

    # Owned by Flywheel or not
    df["fw_historic"] = df.apply(
        lambda row: is_flywheel_maintained(row["source"])
        or is_flywheel_maintained(row["url"]),
        axis=1,
    )

    # In gitlab.com/flywheel-io/scientific-solutions/gears or not
    df["needs_migration"] = (
        df.apply(
            lambda row: needs_migration(row["source"]) and needs_migration(row["url"]),
            axis=1,
        )
        & df["fw_historic"]
    )

    df.sort_values(by=["name", "modified"], inplace=True)
    df.drop_duplicates(subset=["name"], keep="last", inplace=True)

    print_summary_report(df)

    df.to_csv(output_csv, index=False)
    log.info(f"Inventory saved to {output_csv}")


if __name__ == "__main__":
    main()
