# Development and Utilities Scripts

A set of utility scripts are provided in the *scripts* directory. These scripts are
intended to be run from `maintenance` directory.

## Getting started

Before running any scripts, ensure you are in the `maintenance` directory. Navigate to
this directory from the `gear-exchange` repository root with the command:

```bash
cd maintenance
```

Install the project using poetry:

```bash
poetry install
```

## List of Python Scripts

* [add-gear-categorization.py](scripts/add-gear-categorization.py)
* [export-inventory.py](scripts/export-inventory.py)
* [remove-gear.py](scripts/export-inventory.py)
* [bulk-update-gear.py](scripts/bulk-update-gear.py)
* [utils.py](scripts/export-inventory.py)

## Usage

### Export Inventory (export-inventory.py)

This script exports the inventory of gears and boutiques to a CSV file.

```bash
poetry run ./scripts/export-inventory.py
```

### Remove a gear (remove-gear.py)

This script removes a gear from the repository. It will remove the gear from the
inventory.

```bash
poetry run ./scripts/remove-gear.py <gear-name>
```

### Add Gear Categorization (add-gear-categorization.py)

This script ...

### Bulk Update Gear (bulk-update-gear.py)

This script ...

### Utils (utils.py)

This script ...

<!-- TODO add instruction for the shell script -->
<!-- TODO example data attach that can be used as example for user? -->